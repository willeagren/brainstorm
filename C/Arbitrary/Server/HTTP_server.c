/*
    Author: Wilhelm Ågren, wagren@kth.se
    Last modified: 19/03/2020
*/
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <unistd.h> 

#define PORT 8787
#define TRUE 1
#define FALSE 0

void handle_client() {
    int server_fd, new_socket;
    long valread;
    struct sockaddr_in addr;
    int addrlen = sizeof(addr);

    char* http_response = "HTTP/1.1 200 OK\nServer: jomega@bund\nContent-Type: text/plain\nContent-Length: 50\n\nTicking away. The moments that make up a dull day.";

    if((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == FALSE) {
        perror("In socket");
        exit(EXIT_FAILURE);
    }

    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(PORT);

    memset(addr.sin_zero, '\0', sizeof(addr.sin_zero));

    if(bind(server_fd, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
        perror("In bind");
        exit(EXIT_FAILURE);
    }

    if(listen(server_fd, 10) < 0) {
        perror("In listen");
        exit(EXIT_FAILURE);
    }

    while(TRUE) {
        printf("\n +++++++++++++ [Waiting for new connection] +++++++++++++ \n\n");
        if((new_socket = accept(server_fd, (struct sockaddr*)&addr, (socklen_t*)&addrlen)) < 0) {
            perror("In accept");
            exit(EXIT_FAILURE);
        }

        char buf[30000] = {0};
        valread = read(new_socket, buf, 30000);
        printf("%s\n", buf);
        write(new_socket, http_response, strlen(http_response));
        printf(" ---------------- [Welcome message sent] ----------------\n");
        close(new_socket);
    }
}

// Func main/2
int main(int argc, char** argv) {
    handle_client();
    return 0;
}