#!/bin/bash
echo "Estimating pi using a Leibniz formula."
rm bench_leibniz_pi.dat
touch bench_leibniz_pi.dat
echo "Compiling code..."
gcc -o leib_pi leibniz_pi.c
echo "Done compiling."
echo "Executing..."
for((i = 1; i < 10000; i++))
do
	echo -n "$i" >> bench_leibniz_pi.dat
	./leib_pi "$i" | tee -a bench_leibniz_pi.dat
	echo -n -e "\n" >> bench_leibniz_pi.dat
done
echo -e "\nComputation completed."
