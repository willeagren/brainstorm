#!/bin/bash
echo "Estimating pi using a Monte Carlo algorithm"
rm bench_mcarlo_pi.dat
touch bench_mcarlo_pi.dat
echo "Compiling code..."
gcc -o mcarlo_pi monte_carlo_pi.c
echo "Done compiling."
echo "Executing..."
for((i = 1; i < 100000; i++))
do
	echo -n "$i" >> bench_mcarlo_pi.dat
	./mcarlo_pi "$i" | tee -a bench_mcarlo_pi.dat
	echo -n -e "\n" >> bench_mcarlo_pi.dat
done
echo -e "\nComputation completed."
