/*
    Code written to compare this well-known formula
    to a Monte Carlo approach to estimating pi. 
    Using the Leibniz formula for approximation.

    Author: Wilhelm Ågren, wagren@kth.se
    Last modified: 19/03/2020
*/
#include <stdio.h>
#include <stdlib.h>

// Func leibniz_formula_pi/1 takes an integer n that specifies
// how many times the sum should be iterated. Function will return
// a double which wll be the estimation of pi, depending on n.
//
// More info regarding the Leibniz formula for pi can be found
// at the gitlab page, with corresponding sum notation.
double leibniz_formula_pi(int n) {
    double pi = 0;
    int i;
    int pol = 1;
    for(i = 0; i <=n; i++) {
        pi += (double)(pol*1)/(2*i + 1);
        pol *= -1;
    }
    return pi*4;
}

// Func print_argv/2 takes main arguments to print for debugging.
void print_argv(int argc, char** argv) {
    int i;
    for(i = 0; i < argc; i++) {
        printf("Argument [%d]: %s\n", i + 1, *(argv + i));
    }
    return;
}

// Func main/2 takes the number of iterations as an argument
// and then approximates pi based on that number.
// First argument specifies how many arguments there are in
// char** argv and the second argument is a pointer that holds
// the arguments. 
int main(int argc, char** argv) {
    if(argc != 2) {
        printf("[ERROR] Incorrect argument formating. Printing argv:\n");
        print_argv(argc, argv);
        return 1;
    } else {
        int arg;
        sscanf(*(argv + 1), "%d", &arg);
        printf(" %lf\n", leibniz_formula_pi(arg));
    }
    return 0;
}