/*
    Code written partly for a daily_coding challenge,
    but also because I wanted to see how well this
    method of approximating pi works. It does not work
    too good in C, since the lib func rand() generates
    pseudorandom numbers based on a seed.

    Author: Wilhelm Ågren, wagren@kth.se
    Last modified: 17/03/2020
*/
#include <stdio.h>
#include <stdlib.h>

// Func estimate_pi/1 takes an integer n as argument that 
// declares how many time he approximation should
// be iterated. Function returns a double num that
// is the approximated value to pi.
//
// The area of a circle is pi*r² and the area of a square with
// length of sides equal to 2r is (2r)². 
//
// Randomizing n numbers inside either both the circle and square 
// or just the square and then dividing the number of points 
// in the circle with the number of points in side the square 
// will yield a fraction that is: pi/4
//
// Function returns this fraction*4 to approximate pi!
double estimate_pi(int n) {
    int p_circle = 0;
    int p_square = 0;
    int i;
    for(i = 0; i < n; i++) {
        double x = (double)rand() / (double)RAND_MAX;
        double y = (double)rand() / (double)RAND_MAX;
        double r = (x*x + y*y);
        if(r <= 1) p_circle++;
        p_square++;
    }
    return (double)4*((double)p_circle/(double)p_square);
}

// Func debug_argv/2 takes main arguments to print for debugging.
void debug_argv(int argc, char** argv) {
    int i;
    for(i = 0; i < argc; i++) {
        printf("Argument [%d]: %s\n", i + 1, *(argv + i));
    }
    return;
}

// Func main/2 takes the number of iterations as an argument
// and then approximates pi based on that number.
int main(int argc, char** argv) {
    if(argc != 2) {
        printf("[ERROR] Incorrect argument formating. Printing argv:\n");
        debug_argv(argc, argv);
        printf("[ERROR] Aborting...\n");
        return 1;
    } else {
        int arg;
        sscanf(*(argv + 1), "%d", &arg);
        printf(" %lf\n", estimate_pi(arg));
    }
    return 0;
}