/*
    Code written because I was bored and wanted something to
    do while I was traveling home from Visby. 

    Author: Wilhelm Ågren, wagren@kth.se
    Last modified: 17/03/2020
*/

#include <stdio.h>
#include <stdlib.h>

// Func calc_pascal/1 takes an integer specifying the depth/
// number of rows & columns in the triangle. Calulates all the 
// pascal numbers/fibonacci numbers of the triangle using 
// dynamic programming. 
//
// Methodically, the implementation is done using an int* that 
// can be interpreted as an array. Instead of having a 2D array
// with every row & column for the triangle, we use the int*
// to dynamically allocate as much needed space as a 2D array but 
// it will physically be represented as a single array in the following way:
//
// Row:        1     2     3  
//          |_____|_____|_____|
// int* ->  |_|_|_|_|_|_|_|_|_|
//
// This give us a 3x3 2D array, but represented by a single array
// that has length 9. This way we can just increment the pointer 
// accordingly to find the correct adresses.
//
// Given a triangle with depth n,
// Timecomplexity: T(n) ∈ O(n²)
// Memory complexity: Quadratic
int* calc_pascal(int n) {
    int* arr = malloc(sizeof(int*) * n * n);
    int i, j;
    for(i = 0; i < n; i++) {
        for(j = 0; j <= i; j++) {
            if(j == i || j == 0) {
                *(arr + i*n + j) = 1;
            } else {
                *(arr + i*n + j) = *(arr + (i-1)*n + (j-1)) + *(arr + (i-1)*n + j);
            }
        }
    }
    return arr;
}

// Func print_triangle/2 takes an int pointer to the start of the 
// array that holds all of the pascal values and an integer specifying
// the depth/number of rows & columns of the triangle
// 
// Given a triangle with depth n,
// Timecomplexity: T(n) ∈ O(n²)
// Memory complexity: Constant
void print_triangle(int* arr, int n) {
    int i, j, k;
    for(i = 0; i < n; i++) {
        for(k = (n - i); k > 0; k--) {
            printf(" ");
        }
        for(j = 0; j <= i; j++) {
            printf("[%d]", *(arr + i*n + j));
        }
        printf("\n");
    }
}

// Func main/0 used to test program.
// Exits on completion.
int main() {
    int n;
    printf("Input the depth of the triangle: \n");
    scanf("%d", &n);
    int* triangle = calc_pascal(n);
    print_triangle(triangle, n);
    return 0;
}