/*
    Sorting algorithm for positive integers, where 
    the largest integer is guaranteed to be found
    in the last position of the array that is 
    to be sorted. Sorts in linear time but has 
    atrocious memory complexity.

    Author: Wilhelm Ågren, wagren@kth.se
    Last modified: 20/03/2020
*/
#include <stdlib.h>
#include <stdio.h>

// Func ptr_sort/2 takes two arguments. First one 
// is a list 'l' of unsorted positive integers and the second
// argument is an integer 'len' that is the length of the list.
// Function returns a new sorted list, allocated on the heap
// using malloc(). The amount of memory allocated on the heap
// is decided by the largest integer in the list 'l', thus
// creating a large amount of internal fragmentation.
//
// Given a list 'l' of length 'n' and max(l) = 'm':
// Timecomplexity: T(n) ∈ O(n)
// Memory complexity: Linear with respect to m
int* ptr_sort(int* l, int len) {
    int i;
    int max = *(l + len - 1);
    int* ptr_sort = (int*)malloc(sizeof(int)*max);
    for(i = 0; i < len; i++) {
        *(ptr_sort + *(l + i)) = *(l + i);
    }
    return ptr_sort;
}

// Func print_sort/2 takes two arguments. First one is 
// the sorted list 'l' and the second one is an integer 'max'
// that is the length of the dynamically allocated memory.
// Basically just a wrapper for printf().
void print_sort(int* l, int max) {
    int i;
    for(i = 0; i <= max; i++) {
        if(*(l + i) == NULL) continue;
        printf("[%d]", *(l + i));
    }
    printf("\n");
    return;
}

// Func main/0 used a testing method for the sorting algorithm.
// -ignore
int main() {
    int arr[15] = {3,1,7,2,6,4,2,8,5,9,14,12,18,17,20};
    int* l = &arr[0];
    int len = 15;
    int* sorted = ptr_sort(l, len);
    print_sort(sorted, *(l + len - 1));
    free(sorted);
    return 0;
}