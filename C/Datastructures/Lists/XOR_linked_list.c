#include <stdlib.h>
#include <stdio.h>


int main() {

    struct node *tail = NULL; 
    // Build: tail --> 10 <--> 2 <--> 40 <--> -16
    insert_node(&tail, 10);
    print_list(tail);
    insert_node(&tail, 2);
    print_list(tail);
    insert_node(&tail, 40);
    print_list(tail);
    return 0;
}