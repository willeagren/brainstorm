#include <stdlib.h>
#include <stdio.h>

struct node {
    int data;
    struct node* next; // XOR of next and prev
};

struct node* XOR(struct node* p, struct node* n) {
    return (struct node*) ((int) (p) ^ (int) (n));
}

void insert_node(struct node** tail, int data) {
    printf("Insert node...\n");
    struct node* new_node = (struct node*) malloc(sizeof(struct node));
    new_node->data = data;
    new_node->next = XOR(NULL, *tail);
    if(*tail == NULL) {
        (*tail) = new_node;
        printf("Successfull insertion\n");
        return;
    }
    struct node* next = XOR(NULL, ((*tail)->next));
    (*tail)->next = XOR(new_node, next);
    *tail = new_node;
    printf("Successfull insertion\n");
    return;
}

void print_list(struct node* tail) {
    printf("printing\n");
    struct node* tmp = tail;
    struct node* prev = NULL;
    struct node* next;
    while(tmp != NULL) {
        printf("Inside loop...\n");
        printf("[%d] - ", tmp->data);
        next = XOR(prev, tmp->next);
        prev = tmp;
        tmp = next;
    }
    printf("\n");
    return;
}