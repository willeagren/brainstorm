/*
    Program and API for a Binary Search Tree.
    Supports interactive mode, where the user
    can initialize and modify the BST in terminal
    through commands. Most of the commands are
    curently under work. Supports: init, add, print

    Author: Wilhelm Ågren, wagren@kth.se
    Last modified: 19/03/2020
*/
#include <stdlib.h>
#include <stdio.h>
#include "binary_tree.h"
#include <string.h>

// Limit for fgets() buffer
#define MAX_LIMIT 20

// Func init_tree/1
struct node* init_tree(int data) {
    return new_node(data);
}

// Func add_element/2 takes a node* root and integer data as arguments.
// node* root is used to traverse the tree and find the correct
// position for the new element 'data' to be added.
// Returns the root node, with modified children.
struct node* add_element(struct node* root, int data) {
    if(root == NULL) return new_node(data);
    // Left leaning if element added is equal to node root element
    if(data <= root->data) {
        root->left = add_element(root->left, data);
    } else {
        root->right = add_element(root->right, data);
    }
    return root;
}

// Func print_tree/2 takes pointer node* root to traverse the tree and current
// depth of the tree as arguments. Depth is used to make the print pretty. 
//
// Recursively traverses the tree.
void print_tree(struct node* root, int dep) {
    if(root != NULL) {
        print_tree(root->right, dep + 1);
        int i;
        for(i = 1; i < dep*dep; i++) {
            printf(" ");
        }
        printf("[%d]\n", root->data);
        print_tree(root->left, dep + 1);
    }
}

// Func is_numer/2 takes two arguments. A char*, string, and an integer len.
// Returns 0 if the string solely consists of integers, and 1 otherwise.
int is_number(char* c, int len) {
    int i;
    for(i = 0; i < len; i++) {
        if(!isdigit(*(c + i))) return 0;
    }
    return 1;
}

// Func main/2 that handles evaluation of execution flag and likewise.
// Supports interactive exeuction.
int main(int argc, char** argv) {
    struct node* root;
    if(argc == 2) {
        char tag = **(argv + 1);
        if(tag == 'i') {
            printf("--== [Interactive] ==--\n    >> Initialize the tree with a value [v]: init *enter* v\n    >> Add an element with value [v] to the tree: add *enter* v\n    >> Delete an element with value [v] from the tree: delete *enter* v\n    >> Print the tree: print\n    >> Quit interactive mode: q\n");
            while(1) {
                char user_s[MAX_LIMIT];
                fgets(user_s, MAX_LIMIT, stdin);
                // Remove '\n' from user String
                if(*(user_s + strlen(user_s) - 1) == '\n') *(user_s + strlen(user_s) - 1) = '\0';
                if(is_number(user_s, strlen(user_s))) continue;
                printf(".\n");
                if(strcmp(user_s, "init") == 0) {
                    printf("    >>Initializing tree...\n");
                    int v;
                    char* c = getchar();
                    v = (int)c - 48;
                    root = init_tree(v);
                    printf("    >>Initialized tree\n");
                } else if(strcmp(user_s, "add") == 0) {
                    printf("    >>Adding element to tree...\n");
                    int v;
                    char c = getchar();
                    v = (int)c - 48;
                    root = add_element(root, v);
                    printf("    >>Added element to the tree\n");
                } else if(strcmp(user_s, "delete") == 0) {
                    printf("    >>Deleting element from tree...\n");
                    int v;
                    char c = getchar();
                    v = (int)c - 48;
                    // root = delete_element(root, v);
                } else if(strcmp(user_s, "print") == 0) {
                    printf("    >>Printing the tree in order element size...\n");
                    print_tree(root, 1);
                } else if(strcmp(user_s, "q") == 0) {
                    break;
                } else {
                    printf("Unknown command. Try again...\n");
                }

            }
        } else {
            printf("[ERROR] Unknown executable tag: [%s]. Default execution...\n", *(argv + 1));
        }
    }
    return 0;
}