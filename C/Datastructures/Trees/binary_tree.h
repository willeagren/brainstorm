// Include file for binary search tree
struct node
{
    int data;
    struct node* left;
    struct node* right;
};

struct node* new_node(int data) {
    struct node* new = (struct node*)malloc(sizeof(struct node));
    new->left = NULL;
    new->right = NULL;
    new->data = data;
    return new;
}