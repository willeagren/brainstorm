# Welcome, to the domain of segfaults!
I have limited knowledge and experience in C programing, and I'm not afraid to use it. Headscratching low-level computer science is interactive and all... But have you ever done Elixir pattern matching? no jk haha... unless?

## I am the pointer, now _deference_ me...
Estimating pi can be done by comparing the area of a square and the area of a circle. Given a circle with radius **r** and a square with side-length **2r**; the area of the circle is then **pi*r²** and the area of the square is **4r²**. 

Lets say that *r = 1*, and we then randomize **n** coordinates {x,y} inside of the 1x1 square. If the distance from tthe coordinate {x, y} to the origin is less or equal to one, then the coordinate is inside the circle. Otherwise it is just inside the square. By counting the number of coordinates inside the circle and dividing it with the number of coordinates outside of it, we get a fraction that is close to **pi/4**. Since **(pi*r²)/(4*r²)** is the fraction.

Estimating pi using this Monte Carlo method I achieved the following value for pi as n tends to 100000:

![Estimating the value of pi - Monte Carlo](C/Arbitrary/bench_mcarlo_pi.png)
The image was generated using **gnuplot**; the purple line seen in the graph is how the estimated value for pi fluxuates. 

The code can be found in the directory /Arbitrary/ with corresponding bash script.

## 0xFFFF1900AE8C152B or whatever you kids call it these days
A well known formula for calculating pi was found by mathematician Gottfried Leibniz and the sum looks the following way:
![Leibniz sum for calculating pi](C/Arbitrary/leibniz_sum.svg)

Approximating this series with the code found in the file *leibniz_pi* gave me the following value for when the sum converges:
![Estimating the value of pi - Leibniz](C/Arbitrary/bench_leibniz_pi.png)
The image was generated using **gnuplot**; the purple line seen in the graph is how the estimated value for pi converges towards 3,14.