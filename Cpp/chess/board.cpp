#include "board.h"
#include <iostream>

#define WIDTH 8
#define HEIGHT 8

/*
    Constructor of the Board class.
    |-> Sets the castle flags accordingly,
        and places all the pieces on their
        corresponding positions.

    The pieces are represented by a number,
    ranging from 1 -> 6 where white has positive
    integers and black has negative.

    The board initially looks like this:

    8 | -4, -2, -3, -5, -6, -3, -2, -4,
    7 | -1, -1, -1, -1, -1, -1, -1, -1,
    6 |  0,  0,  0,  0,  0,  0,  0,  0,
    5 |  0,  0,  0,  0,  0,  0,  0,  0,
    4 |  0,  0,  0,  0,  0,  0,  0,  0,
    3 |  0,  0,  0,  0,  0,  0,  0,  0,
    2 |  1,  1,  1,  1,  1,  1,  1,  1,
    1 |  4,  2,  3,  5,  6,  3,  2,  4,
         A   B   C   D   E   F   G   H

    The piece coding is:
        |-> 1: Pawn 
        |-> 2: Knight
        |-> 3: Bishop
        |-> 4: Rook
        |-> 5: Queen
        |-> 6: King
*/
Board::Board() {
    b = new Piece* [8];
    int i;
    for(i = 0; i < WIDTH; i++) {
        *(b + i) = new Piece[HEIGHT];
    }

    white_castle = 0;
    black_castle = 0;
    game_in_progress = 1;
}

/*
    Function init_board/0
    @spec init_board() :: void

    Places all the pieces on their starting positions.
*/
void Board::init_board() {
    /* b[1][0] x = 1 y = 0 */
    std::cout << "Initializing board.\n";
    int i, j;

    /* Position all pawns. */
    for(i = 0; i < WIDTH; i++) {
        b[i][1].set_piece(1, 1);
        b[i][6].set_piece(0, -1);
    }

    /* Position the rooks. */
    b[0][0].set_piece(1, 4);
    b[7][0].set_piece(1, 4);
    b[0][7].set_piece(0, 4);
    b[7][7].set_piece(0, -4);

    /* Position the knights. */
    b[1][0].set_piece(1, 2);
    b[6][0].set_piece(1, 2);
    b[1][7].set_piece(0, -2);
    b[6][7].set_piece(0, -2);

    /* Positions the bishops. */
    b[2][0].set_piece(1, 3);
    b[5][0].set_piece(1, 3);
    b[2][7].set_piece(0, -3);
    b[5][7].set_piece(0, -3);

    /* Positions the queens. */
    b[3][0].set_piece(1, 5);
    b[3][7].set_piece(0, -5);

    /* Position the kings. */
    b[4][0].set_piece(1, 6);
    b[4][7].set_piece(0, -6);

    /* Set the empty squares. */
    for(i = 2; i < 6; i++) {
        for(j = 0; j < WIDTH; j++) {
            b[j][i].set_piece(1, 0);
        }
    }
}

/*
    Function print_board/0
    @spec get_pos() :: void

    Print the board.
*/
void Board::print_board() {
    int i, j;
    for(i = HEIGHT - 1; i >= 0; i--) {
        std::cout << i + 1 << " | ";
        for(j = 0; j < WIDTH; j++) {
            b[j][i].print_piece();
        }
        std::cout << "\n";
    }
    std::cout << "   ------------------------\n";
    std::cout << "     A  B  C  D  E  F  G  H\n";
}

/*
    Function get_game_progress/0
    @spec get_game_progress() :: integer

    Returns the flag for game loop in main.cpp
*/
int Board::get_game_progress() {
    return game_in_progress;
}

/*
    Function is_valid_move/3
    @spec is_valid_move(Move, Piece, Piece**) :: integer

    Returns a 1 if it is a valid move - 0 otherwise.

    Find all possible moves for the piece from the pos x_from y_from.
    If the move m is in the list of possible moves - it is valid.

    Third argument is a 2d array of Pieces - which is used to represent the board.
*/
int is_valid_move(Move m, Piece p, Piece** b) {
    if(!p.get_type()) return 0;
    p.calculate_moves(m.x_from, m.y_from, b);
    std::vector<Move> moves = p.get_moves();
    for(int i = 0; i < p.get_num_moves(); i++) {
        if(moves[i].compare_to(m)) return 1;
    }
    std::cout << "Not a valid move. Please try again.\n";
    return 0;
}

/*
    Function perform_move/1
    @spec perform_move(Move) :: void

    Takes a move and performs it on the board if possible.

    m-
    |-> x_from, y_from, x_to, y_to

    get piece on [x_from, y_from] and place it on [x_to, y_to]
    |-> ALSO put empty piece (0) on [x_from, y_from]
*/
void Board::perform_move(Move m) {
    Piece p = b[m.x_from][m.y_from];
    if(is_valid_move(m, p, b)) {
        b[m.x_to][m.y_to] = p;
        b[m.x_from][m.y_from].set_piece(1, 0);
    }
}