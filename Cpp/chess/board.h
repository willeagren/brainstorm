#include "pieces.h"
//#include "move.h"

#ifndef BOARD_H
#define BOARD_H

/*
    Class definition for the Board class.

    Board class is used to save the current gamesteate.
    |-> Keeping track of all piece positions, and if
        any of the kings have castled.
        Keeping track of castling as to not waste time
        looking for possible castle when it's already made.

    The struct holds in total 8*8 + 2 integers,
    which adds up to (64+2)*4 bytes = 264B

    NOTE:
    |-> Removed Board::get_pos since you can get
        the piece at a position x,y by simply 
        accessing the array b
        b[x][y] returns the piece at the position
*/
class Board {
    public:
        Board();
        void print_board();
        void init_board();
        int get_game_progress();
        void perform_move(Move m);

    private:
        Piece** b;
        int white_castle;
        int black_castle;
        // Set this flag to 0 when checkmate, stalemate or draw.
        int game_in_progress;
};

#endif