#include "board.h"
#include "pieces.h"
#include "move.h"
#include <iostream>
#include <string>
#include <stdlib.h>

#define WIDTH 8
#define HEIGHT 8

using namespace std;

/*
	Function translate_move/1
	@spec translate_move(character) :: integer

	Translate the character of the coordinate to an integer.
	Function returns an integer greater or equal to zero if
	the character matches a valid coordinate. OTHERWISE returns -1.
*/
int translate_move(char c) {
	int i;
	char translate_lookup_x[WIDTH] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'g'};
	char translate_lookup_y[WIDTH] = {'1', '2', '3', '4', '5', '6', '7', '8'};
	for(i = 0; i < WIDTH; i++) {
		if(c == *(translate_lookup_x + i)) return i;
		if(c == *(translate_lookup_y + i)) return i;
	}
	
	return -1;
}

/*
	Function is_valid_input/4
	@spec is_valid_move(integer, integer, integer, integer) :: integer

	Takes the move coordinates and validates that they are on the board - not out of bounds.
	Returns 1 if satisfied - 0 otherwise.
*/
int is_valid_input(int x_from, int y_from, int x_to, int y_to) {
	return 8 > x_from >= 0 && 8 > y_from >= 0 && 8> x_to >= 0 && 8 > y_to >=0;
}


/*
	Function print_game_history/1
	@spec print_game_history(std::vector<Move>) :: void

	Takes a list of the played game-moves and prints them accordingly.
*/
void print_game_history(vector<Move> game_moves) {
	for(vector<Move>::iterator it = game_moves.begin(); it != game_moves.end(); ++it) {
		Move m = *it;
		m.print_move();
	}
}

/*
	Function print_help/0
	@spec print_help() :: void

	Prints all available commands to the CLI -
	also shows how a valid move command should look like.

	WIP
*/
void print_help() {
	cout << "This is the help flag.\n";
}


/*
	Function get_player_move/0
	@spec get_player_move() :: Move

	Returns the formatted move according to common chess notation.
	e.g. e2 e4

	Hanlding of game-history or -helper flags are done in thsi function.
*/
Move get_player_move(vector<Move> game_moves) {
	Move m;
	string s;
	cout << "Please enter your move...\n";
	getline(cin, s);
	if(s == "history") {
		print_game_history(game_moves);
		get_player_move(game_moves);
	}
	if(s == "-h") {
		print_help();
		get_player_move(game_moves);
	}

	/* Subtract 1 from the y-coordinates as we are working in 0indexed world*/
	int x_from = translate_move(s.at(0));
	int y_from = translate_move(s.at(1));
	int x_to = translate_move(s.at(3));
	int y_to = translate_move(s.at(4));
	if(is_valid_input(x_from, y_from, x_to, y_to)) {
		m.set_move(x_from, y_from, x_to, y_to);
	} else {
		get_player_move(game_moves);
	}
	return m;
}

int main(int argc, char** argv) {

	vector<Move> game_moves;
	Board b;
	b.init_board();
	Move m;
	while(b.get_game_progress()) {
		/* Linux only - use system("CLR"); on windows systems. */
		system("clear");
		b.print_board();
		m = get_player_move(game_moves);
		b.perform_move(m);
		game_moves.insert(game_moves.begin(), m);
		b.print_board();
		m = get_player_move(game_moves);
		b.perform_move(m);
		game_moves.insert(game_moves.begin(), m);
	}
	return 0;
}