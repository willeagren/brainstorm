#include "move.h"
#include "pieces.h"
#include <iostream>

/* The default constructor initializes the move as [0,0] -> [0,0]. */
Move::Move() {
    x_from = 0;
    y_from = 0;
    x_to = 0;
    y_to = 0;
}

void Move::set_move(int xf, int yf, int xt, int yt) {
    x_from = xf;
    y_from = yf;
    x_to = xt;
    y_to = yt;
}

void Move::print_move() {
    char translate_lookup[8] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'g'};
    std::cout << translate_lookup[x_from] << y_from + 1 << " " << translate_lookup[x_to] << y_to + 1 << "\n";
}

int Move::compare_to(Move other_move) {
    return x_from == other_move.x_from && y_from == other_move.y_from && x_to == other_move.x_to && y_to == other_move.y_to;
}