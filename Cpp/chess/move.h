#include <vector>

#ifndef MOVE_H
#define MOVE_H

/*
    Class definition for the Move class.

    The move class is used to format player moves, and
    is primarely created to be a wrapper for the 4 integers -
    |-> x_from, y_from , x_to, y_to

    The class currently provides these functions:
        void set_move/4
            |-> which sets the four local variables

        void print_move/0
            |-> which prints the move in standard chess format

        int compare_to/1
            |-> compare another move to the local move. Returns 1 if they are the same - 0 otherwise.
*/
class Move {
    public:
        Move();
        void set_move(int x_f, int y_f, int xt, int yt);
        void print_move();
        int compare_to(Move other_move);
    
        int x_from;
        int y_from;
        int x_to;
        int y_to;
};

#endif