#include "pieces.h"
#include "move.h"
#include <iostream>
#include <cmath>
#include <vector>

#define WIDTH 8
#define HEIGHT 8

/* The default constructor will initiate all pieces as 0, which means empty square. */
Piece::Piece() {
    colour = 1;
    type = 0;
    num_moves = 0;
}

/*
    Function set_piece/2
    @spec set_piece(integer, integer) :: void

    Set the colour 'c' and type 't' of the piece.
*/
void Piece::set_piece(int c, int t) {
    colour = c;
    type = t;
}

/*
    Function get_type/0
    @spec get_type() :: integer

    Returns the type of the piece according to the piece integer coding.
*/
int Piece::get_type() {
    /* CURRENT SEGFAULT FOUND - MIIGHT NOT ALWASY INIT TYPE VAL??*/
    return type;
}

/*
    Function get_colour/0
    @spec get_colour() :: integer

    Returns the colour of the piece - 1 is white 0 is black.
*/
int Piece::get_colour() {
    return colour;
}

/*
    Function print_piece/0
    @spec print_piece() :: void

    Prints the piece accordingly.
*/
void Piece::print_piece() {
    const char* tmp_colour = ((colour) ? " " : "");
    std::cout << " " << tmp_colour << type;
}

/*
    Function in_boundary/4
    @spec in_boundary(integer, integer, integer, integer) :: integer

    Returns whether or not 4 coordinates all are legal on the chess board.
    1 if true, 0 otherwise.
*/
int in_boundary(int x_from, int y_from, int x_to, int y_to) {
    return ((8 > x_from && x_from >= 0) && (8 > y_from && y_from >= 0) && (8 > x_to && x_to >= 0) && (8 > y_to && y_to >= 0));
}

/*
    Function validate_move/3
    @spec validate_move(Move, Piece*, Piece**) :: integer

    Takes a move, a piece, the board and validates wether or 
    not the move m of the piece p is legal on the board b.

    Returns 1 if legal, 0 otherwise.
*/
int validate_move(Move m, Piece* p, Piece** b) {
    /* Hardcoded check to see if the requested move is outside of the board boundaries. */
    std::cout << "Printing move in validate_move/3\n    |-> ";
    m.print_move();
    if(!in_boundary(m.x_from, m.y_from, m.x_to, m.y_to)) return 0;
    int type = p->get_type();
    int colour = p->get_colour();
    Piece tmp_p = b[m.x_to][m.y_to];
    /* If there is a piece at the position we are looking at - continue with validation at cur pos. */
    if(tmp_p.get_type() != 0) {
        std::cout << "The type is: " << tmp_p.get_type() << "\n";
        /* Pawns can't capture forward. */
        if(abs(type) == 1) {
            return 0;
        }
        /* If the piece at the cur pos is different from the Piece 'p' about to make a move - */
        if(tmp_p.get_colour() == p->get_colour()) {
            return 0;
        }
    }

    /* We went through all positions - no pieces were in the way - must be a valid move? */
    return 1;
}

/*
    Function calculate_moves/2
    @spec calculate_moves(integer, integer) :: void

    Function calculates all moves for the Piece from the position [x,y].
    Does not return anything - as all found moves are stored in the private Piece
    variable 'movelist'. The movelist is of type std::vector<Move> and is
    continually updated with insert/2 during the calculation process.
*/
void Piece::calculate_moves(int x, int y, Piece** b) {
    Move m;
    Piece p;
    int dir;
    switch (abs(type)) {
        /* No piece selected. */
        case 0:
            std::cout << "No piece selected. Please enter again.\n    |-> move format is: [x_from, y_from] [x_to, y_to] e.g. e2 e4\n";
            return;
        /* Pawn selected. */
        case 1:
            dir = 1;
            /* White pieces move UP the board - black pieces move DOWN*/
            if(!colour) dir = -1;

            /* The pawns can move two steps up if they are at their starting positions. */
            if((y == 1 && dir > 0)||(y == 6 && dir < 0)) {
                /* [LEGACY COMMENT] Can't do this on one line for some reason. Utterly idiotic.?????? */
                for(int i = 1; i <= 3; i++) {
                    if(i == 3) {
                        movelist.insert(movelist.begin(), m);
                        break;
                    }
                    m.set_move(x, y, x, y + dir*i);
                    if(!validate_move(m, this, b)) break;
                }
            }

            /* Standard move in the direction of the colour. */
            m.set_move(x, y, x, y + dir);
            if(validate_move(m, this, b)) movelist.insert(movelist.begin(), m);

            /* Capture move. */
            m.set_move(x, y, x + dir, y + dir);
            if(validate_move(m, this, b)) movelist.insert(movelist.begin(), m);
            m.set_move(x, y, x - dir, y + dir);
            if(validate_move(m, this, b)) movelist.insert(movelist.begin(), m);
            break;
        
        /* Knight selected. */
        case 2:
            m.set_move(x, y, x + 1, y + 2);
            if(in_boundary(m.x_from, m.y_from, m.x_to, m.y_to)) {
                p = b[x + 1][y + 2];
                if(p.get_type() == 0 || p.get_colour() != colour) movelist.insert(movelist.begin(), m); 
            }

            m.set_move(x, y, x + 2, y + 1);
            if(in_boundary(m.x_from, m.y_from, m.x_to, m.y_to)) {
                p = b[x + 2][y + 1];
                if(p.get_type() == 0 || p.get_colour() != colour) movelist.insert(movelist.begin(), m); 
            }

            m.set_move(x, y, x + 2, y - 1);
            if(in_boundary(m.x_from, m.y_from, m.x_to, m.y_to)) {
                p = b[x + 2][y - 1];
                if(p.get_type() == 0 || p.get_colour() != colour) movelist.insert(movelist.begin(), m); 
            }

            m.set_move(x, y, x + 1, y - 2);
            if(in_boundary(m.x_from, m.y_from, m.x_to, m.y_to)) {
                p = b[x + 1][y - 2];
                if(p.get_type() == 0 || p.get_colour() != colour) movelist.insert(movelist.begin(), m); 
            }

            m.set_move(x, y, x - 1, y - 2);
            if(in_boundary(m.x_from, m.y_from, m.x_to, m.y_to)) {
                p = b[x - 1][y - 2];
                if(p.get_type() == 0 || p.get_colour() != colour) movelist.insert(movelist.begin(), m); 
            }

            m.set_move(x, y, x - 2, y - 1);
            if(in_boundary(m.x_from, m.y_from, m.x_to, m.y_to)) {
                p = b[x - 2][y - 1];
                if(p.get_type() == 0 || p.get_colour() != colour) movelist.insert(movelist.begin(), m); 
            }

            m.set_move(x, y, x - 2, y + 1);
            if(in_boundary(m.x_from, m.y_from, m.x_to, m.y_to)) {
                p = b[x - 2][y + 1];
                if(p.get_type() == 0 || p.get_colour() != colour) movelist.insert(movelist.begin(), m); 
            }

            m.set_move(x, y, x - 1, y + 2);
            if(in_boundary(m.x_from, m.y_from, m.x_to, m.y_to)) {
                p = b[x - 1][y + 2];
                if(p.get_type() == 0 || p.get_colour() != colour) movelist.insert(movelist.begin(), m); 
            }
            break;

        /* Bishop selected. */
        case 3:
            int i;
            /* Up right diagonal */
            for(i = 1; i < 8; i++) {
                m.set_move(x, y, x + i, y + i);
                if(!validate_move(m, this, b)) break; 
                movelist.insert(movelist.begin(), m);
            }
            /* Down right diagonal */
            for(i = 1; i < 8; i++) {
                m.set_move(x, y, x + i, y - i);
                if(!validate_move(m, this, b)) break; 
                movelist.insert(movelist.begin(), m);
            }

            /* Up left diagonal */
            for(i = 1; i < 8; i++) {
                m.set_move(x, y, x - i, y + i);
                if(!validate_move(m, this, b)) break; 
                movelist.insert(movelist.begin(), m);
            }

            /* Down left diagonal */
            for(i = 1; i < 8; i++) {
                m.set_move(x, y, x - i, y - i);
                if(!validate_move(m, this, b)) break; 
                movelist.insert(movelist.begin(), m);
            }
            break;

        default:
            std::cout << "The fuck are you doing...\n";
            break;
    }
    return;
}

/*
    Function get_num_moves/0
    @spec get_num_moves() :: integer

    Returns the size of the private variable movelist using std::vector<T>.size().
*/
int Piece::get_num_moves() {
    return movelist.size();
}

/*
    Function get_moves/0
    @spec get_moves() :: std::vector<Move>

    Returns the movelist.
*/
std::vector<Move> Piece::get_moves() {
    return movelist;
}