#include "move.h"
#include <vector>

#ifndef PIECES_H
#define PIECES_H

/*
    Class definition for the Piece class.

    Base class for all pieces. The piece structure
    holds what type of piece it is and of what colour.

    All Piece objects holds a movelist which encapsulates all available moves for that piece. 
    The movelist is updated each time the piece is selected to move. 
*/
class Piece {
    public:
        Piece();
        void set_piece(int c, int t);
        int get_type();
        int get_colour();
        void print_piece();
        void calculate_moves(int x, int y, Piece** p);
        std::vector<Move> get_moves();
        int get_num_moves();

    private:
        int colour;
        int type;
        int num_moves;
        std::vector<Move> movelist;
};

#endif