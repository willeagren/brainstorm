#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <unistd.h>
#include <string>

/*
 * Random cpp code to learn stuff. 
 * Nothing serious here.
 * No translation lookaside buffer clock algorithms I swear FBI don't steal this.
 * */

using namespace std;

int char_len(char* s) {
	int i;
	int c = 0;
	for(i = 0; s[i]; i++) {
		c++;
	}
	return c;
}

/* Helper function for get_time/0 */
string convert_to_string(char* a, int size) {
	int i;
	string s = "";
	for(i = 0; i < size; i++) {
		s = s + a[i];
	}
	return s;
}

string get_time() {
	time_t now = time(0);
	char* s = ctime(&now);
	int s_size = char_len(s);
	string s_tmp = convert_to_string(s, s_size);
	return s_tmp.erase(s_tmp.length() - 6, 6);
}

void generate_hex_string() {
	char hex_chars[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
	
	int i, j;
	char s[2];
	sleep(1);
	cout << "[" << get_time() << "] |-> ";
	for(i = 0; i < 4; i++) {
		cout << "0x";
		for(j = 0; j < rand()%5 + 1; j++) {
			s[j] = hex_chars[rand()%16];
			cout << s[j];
		}
		cout << " ";
	}
	cout << "\n";
	return;
}

void p_swap(int* p1, int* p2) {
	cout << "Inside p_test/2\n Please enter values for the pointers...\n";
	cin >> *p1;
	cin >> *p2;
	int tmp;
	tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
	cout << "Done swapping.\n";
	return;
}



int main(int argc, char** argv) {
	srand(time(0));
	while(1) {
		generate_hex_string();
	}
	return 0;
}
