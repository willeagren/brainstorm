#include <iostream>
#include <vector>
#include <string>
#include <queue>

int main(int argc, char** argv) {
    std::vector<std::vector<char>> map;
    std::vector<std::vector<int>> visit;
    char input[] = "########\n#   # .#\n#   $$.#\n####   #\n   #@ ##\n   ####";
    int j = 0;
    int i = 0;
    int num_nl = 0;
    while(input[j] != '\0') {
        if(input[j] == '\n') num_nl++;
        j++;
    }
    int rows = num_nl + 1;
    j = 0;
    for(i; i < rows; i++) {
        map.push_back(std::vector<char>());
        visit.push_back(std::vector<int>());
        while(input[j] != '\0') {
            if(input[j] == '\n') {
                j++;
                break;
            }
            std::cout << input[j];
            map[i].push_back(input[j]);
            visit[i].push_back(0);
            j++;
        }
        std::cout << "\n";
    }
    
    int x = 0, y = 0;
    for(i = 0; i < rows; i++) {
        for(j = 0; j < map[i].size(); j++) {
            if(map[i][j] == '@') {
                x = i;
                y = j;
                break;
            }
        }
    }

    std::cout << "Starting sokoban solve...\n";
    std::cout << "    |-> Entry while loop - bismillah!\n";
    std::cout << "    |-> x is: " << x << ", y is: " << y << "\n";
    /* The player coordinates are now at map[i][j] */
    /* Always try and go right, of not then go up, if not then go left - finally go down*/
    int found = 0;
    while(!found) {
        /* We have found the goal - terminate. */
        if(map[x][y] == '.') {
            found = 1;
            break;
        }
        /* We have already been here stupid. Go somewhere else. */
        if(visit[x][y]) {
            x -= 1;
        }
        visit[x][y] = 1;

        /* Go right */
        if(map[x][y + 1] == ' ' || map[x][y + 1] == '.') {
            std::cout << "R";
            if(map[x + 1][y + 1] == '.') {
                std::cout << "\n";
                found = 1;
                continue;
            }
            y += 1;
            continue;
        } else if(map[x - 1][y] == ' ' || map[x - 1][y] == '.') {
            std::cout << "U";
            if(map[x - 1][y] == '.') {
                std::cout << "\n";
                found = 1;
                continue;
            }
            x -= 1;
            continue;
        } else if(map[x][y - 1] == ' ' || map[x][y - 1] == '.') {
            std::cout << "L";
            if(map[x][y - 1] == '.') {
                std::cout << "\n";
                found = 1;
                continue;
            }
            y -= 1;
            continue;
        } else if(map[x + 1][y] == ' ' || map[x + 1][y] == '.') {
            std::cout << "D";
            if(map[x + 1][y] == '.') {
                std::cout << "\n";
                found = 1;
                continue;
            }
            x += 1;
            continue;
        } else {
            std::cout << "NO MOVES LEFT!\n    |-> terminating...\n";
            break;
        }
    }

    return 0;
}