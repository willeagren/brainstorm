#---------------------------------------------------------------------------------------------------------------------------------------------
#Code written by Wilhelm Ågren, last updated: 17/02-2019
#---------------------------------------------------------------------------------------------------------------------------------------------
defmodule Bing do
  def bong(n) do
    cond do
      n < 1 ->
        '*KCHHHHHHHHHHH* Your bingbong is too small! Upgrade or download more RAM, thank you!'
      true ->
      bong(1, n + 1, 1, 1)
    end
  end
  def bong(n, n, _, _) do
    []
  end
  def bong(i, n, 3, 5) do
    [:bingbong | bong(i + 1, n, 1, 1)]
  end
  def bong(i, n, 3, b) do
    [:bing | bong(i + 1, n, 1, b + 1)]
  end
  def bong(i, n, a, 5) do
    [:bong | bong(i + 1, n, a + 1, 1)]
  end
  def bong(i, n, a, b) do
    [i | bong(i + 1, n, a + 1, b + 1)]
  end
end
#---------------------------------------------------------------------------------------------------------------------------------------------
