defmodule Der do
  #@type literal() :: {:const, number()} | {:const, atom()} | {:var, atom()}
  #@type expr() :: {:add, expr(), expr()} | {:mul, expr(), expr()} | literal()
  #Derivative of a constant
  def deriv({:const, _}, _) do
    0
  end
  #Derivative of variable
  def deriv({:var, v}, v) do
    1
  end
  #Derivative of variable with respect to don't_care
  def deriv({:var, y}, _) do
    0
  end
  #Derivative of composite function
  def deriv({:mul, e1, e2}, v) do
    {:add, {:mul, deriv(e1,v), e2}, {:mul, deriv(e2,v), e1}}
  end
  #Derivative of sum of functions
  def deriv({:add, e1, e2}, v) do
    {:add, deriv(e1, v), deriv(e2, v)}
  end
  #Derivative of x^n
  def deriv({:exp, {:var, v}, {:const, n}}, v) do
    {:mul, {:const, n}, {:exp, {:var, v}, {:const, n - 1}}}
  end
  #Derivative of ln(x)
  def deriv({:ln, {:var, v}}, v) do
    {:mul, deriv({:var, v}, v), {:exp, {:var, v}, {:const, -1}}}
  end
  #Derivative of 1/x
  def deriv({:div, {:var, v}}, v) do
    {:mul, {:mul, deriv({:var, v}, v), {:const, -1}}, {:exp, {:var, v}, {:const, -2}}}
  end
  def deriv({:sin, {:var, v}}, v) do
    {:mul, deriv({:var, v}, v), {:cos, {:var, v}}}
  end
  def deriv({:cos, {:var, v}}, v) do
    {:mul, {:mul, {:const, -1}, deriv({:var, v}, v)}, {:sin, {:var, v}}}
  end
end
