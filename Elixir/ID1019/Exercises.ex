defmodule Ex do
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##3 Recursive definitions
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def product(_, 0) do 0 end
  def product(m, n) do m + product(m, n - 1) end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def exp(_, 0) do 1 end
  def exp(x, 1) do x end
  def exp(x, n) do product(x, exp(x, n - 1)) end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def expFast(_, 0) do 1 end
  def expFast(x, 1) do x end
  def expFast(x, n) do
    cond do
      rem(n, 2) == 0 ->
        product(x, exp(x, div(n,2)))
      true ->
        product(x, exp(x, n - 1))
    end
  end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##4.1 Simple functions on lists
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def nth(n, []) do IO.puts("Your requested number is greater than the number of elements in the list, FAILED!") end
  def nth(0, [n | _]) do n end
  def nth(n, [_|t]) do nth(n - 1, t) end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def len(l) do len(l, 0) end
  def len([], len) do len end
  def len([_|t], len) do len(t, len + 1) end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def sum([]) do 0 end
  def sum([h|t]) do h + sum(t) end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def duplicate([]) do [] end
  def duplicate([h|t]) do [h,h|duplicate(t)] end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def add(x, []) do [x] end
  def add(x, [x|t]) do [x|t] end
  def add(x, [h|t]) do [h | add(x, t)] end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def remove(x, []) do [] end
  def remove(x, [x|t]) do remove(x, t) end
  def remove(x, [h|t]) do [h | remove(x, t)] end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def unique(l) do unique(l, []) end
  def unique([], res) do res end
  def unique([h|t], res) do unique(t, add(h, res)) end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def pack(l) do pack(l, l, unique(l), []) end
  def pack(_, _, [], res) do res end
  def pack(ref, [], [_|t], res) do pack(ref, ref, t, res) end
  def pack(ref, [h|t1], [h|t2], res) do pack(ref, t1, [h|t2], [h|res]) end
  def pack(ref, [h|t1], [n|t2], res) do pack(ref, t1, [n|t2], res) end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 def reverse(l) do reverse(l, []) end
 def reverse([], rev) do rev end
 def reverse([h|t], rev) do reverse(t, [h|rev]) end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##4.3 Insertion Sort
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def insert(e, []) do [e] end
  def insert(e, [h|t]) when e < h do [e|[h|t]] end
  def insert(e, [h|t]) when e > h do [h|insert(e,t)] end
  def isort(l) do isort(l, []) end
  def isort([], sorted) do sorted end
  def isort([h|t], sorted) do isort(t, insert(h,sorted)) end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##4.4 Merge Sort
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def msort([]) do [] end
  def msort([h|[]]) do [h] end
  def msort(l) do
    {left, right} = msplit(l, [], [])
    merge(msort(left), msort(right))
  end
  def merge([], right) do right end
  def merge(left, []) do left end
  def merge([l1|r1],[l2|r2]) do
    cond do
      l1 < l2 ->
        [l1|merge(r1,[l2|r2])]
      true ->
        [l2|merge([l1|r1],r2)]
    end
  end
  def msplit([], left, right) do {left, right} end
  def msplit([h|[]], left, right) do msplit([], [h|left], right) end
  def msplit([h1,h2|t], left, right) do msplit(t, [h1|left], [h2|right]) end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##4.5 Quick Sort
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def qsort([]) do [] end
  def qsort([h|[]]) do [h] end
  def qsort([p|t]) do
    {left, right} = qsplit(p, t, [], [])
    small = qsort(left)
    large = qsort(right)
    append(small,[p|large])
  end
  def qsplit(_, [], small, large) do {small, large} end
  def qsplit(p, [h|t], small, large) do
    cond do
      p > h ->
        qsplit(p, t, [h|small], large)
      true ->
        qsplit(p, t, small, [h|large])
    end
  end
  def append([], right) do right end
  def append([h|t], right) do
    [h|append(t, right)]
  end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##6.1 Integer to Binary
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def to_binary(n) do to_binary(n, []) end
  def to_binary(0, b) do b end
  def to_binary(n, b) do to_binary(div(n, 2), [rem(n, 2)|b]) end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##6.2 Binary to Integer
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def to_integer(l) do to_integer(l, 0) end
  def to_integer([], i) do i end
  def to_integer([x|t], i) do to_integer(t,2*i + x) end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##7 Fibonacci
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def fib(0) do {0, :nil} end
  def fib(1) do {1, 0} end
  def fib(n) do
    {fib1, fib2} = fib(n - 1)
    {fib1 + fib2, fib1}
  end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##A 2-3 Tree
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  #@type leaf::{:leaf, key(), value}
  #@type two_node::{:two, key, left, right}
  #@type three_node::{:three, k1, k2, left, middle, right}
  #@type four_node::{:four, k1, k2, k3, left, m1, m2, right}
  def insertf(key, value, :nil) do {:leaf, key, value} end
  def insertf(k, v, {:leaf, k1, _} = l) do
    cond do
      k <= k1 ->
        {:two, k, {:leaf, k, v}, l}
      true ->
        {:two, k1, l, {:leaf, k, v}}
    end
  end
  def insertf(k, v, {:two, k1, {:leaf, k1, _} = l1, {:leaf, k2, _} = l2}) do
    cond do
      k <= k1 ->
        {:three, k, k1, {:leaf, k, v}, l1, l2}
      k <= k2 ->
        {:three, k1, k, l1, {:leaf, k, v}, l2}
      true ->
        {:three, k1, k2, l1, l2, {:leaf, k, v}}
    end
  end
  def insertf(k, v, {:three, k1, k2, {:leaf, k1, _} = l1, {:leaf, k2, _} = l2, {:leaf, k3, _} = l3}) do
    cond do
      k <= k1 ->
        {:four, k, k1, k2, {:leaf, k, v}, l1, l2, l3}
      k <= k2 ->
        {:four, k1, k, k2, l1, {:leaf, k, v}, l2, l3}
      k <= k3 ->
        {:four, k1, k2, k, l1, l2, {:leaf, k, v}, l3}
      true ->
        {:four, k1, k2, k3, l1, l2, l3, {:leaf, k, v}}
    end
  end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##AVL tree
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  #@type tree::{:node, key(), value(), diff(), tree(), tree()} | :nil
  def insert(tree, key, value) do
    case insrt(tree, key, value) do
      {:inc, q} ->
        q
      {:ok, q} ->
        q
    end
  end
  defp insrt(:nil, key, value) do {:inc, {:node, key, value, 0, :nil, :nil}} end
  defp insrt({:node, key, _, f, left, right}, key, value) do {:ok, {:node, key, value, f, left, right}} end
  ##Insert in the left branch
  defp insrt({:node, rk, rv, 0, left, right}, key, value) when key < rk do
    case insrt(left, key, value) do
      {:inc, q} ->
        {:inc, {:node, key, value, -1, q, right}}
      {:ok, q} ->
        {:ok, {:node, key, value, 0, q, right}}
    end
  end
  ##Insert in the right branch
  defp insrt({:node, rk, rv, 0, left, right}, key, value) when key > rk do
    case insrt(left, key, value) do
      {:inc, q} ->
        {:inc, {:node, key, value, +1, left, q}}
      {:ok, q} ->
        {:ok, {:node, key, value, 0, left, q}}
    end
  end
  ##Insert in the left branch but in a tree that has a deeper right branch
  defp insrt({:node, rk, rv, +1, left, right}, key, value) when key < rk do
    case insrt(left, key, value) do
      {:inc, q} ->
        {:inc, {:node, key, value, 0, q, right}}
      {:ok, q} ->
        {:ok, {:node, key, value, +1, q, right}}
    end
  end
  ##Insert in the right branch but in a tree that has a deeper right branch, this causes a rotation
  defp insrt({:node, rk, rv, +1, left, right}, key, value) when key > rk do
    case insrt(left, key, value) do
      {:inc, q} ->
        {:inc, rotate({:node, key, value, +2, left, q})}
      {:ok, q} ->
        {:ok, {:node, key, value, +1, left, q}}
    end
  end
  ##Insert in the left branch but in a tree that has a deeper left branch, this causes a rotation
  defp insrt({:node, rk, rv, -1, left, right}, key, value) when key < rk do
    case insrt(left, key, value) do
      {:inc, q} ->
        {:inc, rotate({:node, key, value, -2, q, right})}
      {:ok, q} ->
        {:ok, {:node, key, value, -1, q, right}}
    end
  end
  ##Insert in the left branch but in a tree that has a deeper left branch
  defp insrt({:node, rk, rv, -1, left, right}, key, value) when key > rk do
    case insrt(left, key, value) do
      {:inc, q} ->
        {:inc, {:node, key, value, 0, left, q}}
      {:ok, q} ->
        {:ok, {:node, key, value, -1, left, q}}
    end
  end
  ##Single rotation
  defp rotate({:node, xk, xv, -2, {:node, yk, yv, -1, a, b}, c}) do
    {:node, yk, yv, 0, a, {:node, xk, xv, 0, b, c}}
  end
  ##Single rotation
  defp rotate({:node, xk, xv, -2, a, {:node, yk, yv, +1, b, c}}) do
    {:node, yk, yv, 0, {:node, xk, xv, 0, a, b}, c}
  end
  ##Double rotation
  defp rotate({:node, xk, xv, -2, {:node, yk, yv, +1, a, {:node, zk, zv, -1, b, c}}, d}) do
    {:node, zk, zv, 0, {:node, yk, yv, 0, a, b}, {:node, xk, xv, +1, c, d}}
  end
  ##Double rotation
  defp rotate({:node, xk, xv, -2, {:node, yk, yv, +1, a, {:node, zk, zv, +1, b, c}}, d}) do
    {:node, zk, zv, 0, {:node, yk, yv, -1, a, b}, {:node, xk, xv, 0, c, d}}
  end
  ##Double rotation
  defp rotate({:node, xk, xv, -2, {:node, yk, yv, +1, a, {:node, zk, zv, 0, b, c}}, d}) do
    {:node, zk, zv, 0, {:node, yk, yv, 0, a, b}, {:node, xk, xv, 0, c, d}}
  end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##Tentamen 2015-03-13 Uppgift 2.2: Pop from the top of the heap
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  @type heap::{:node, integer(), heap(), heap()} | :nil
  def pop(:nil) do :false end
  def pop({:node, value, :nil, :nil}) do {value, :nil} end
  def pop({:node, value, :nil, right}) do {value, right} end
  def pop({:node, value, left, :nil}) do {value, left} end
  def pop({:node, value, {:node, lv, _, _} = left, {:node, rv, _, _} = right}) do
    cond do
      lv > rv ->
        {lv, rest} = pop(left)
        {value, {:node, lv, rest, right}}
      true ->
        {rv, rest} = pop(right)
        {value, {:node, rv, left, rest}}
    end
  end
  def insert(e, :nil) do {:node, e, :nil, :nil} end
  def insert(e, {:node, v, left, right}) when e > v do {:node, e, right, insert(v, left)} end
  def insert(e, {:node, v, left, right}) when e < v do {:node, v, right, insert(e, left)} end
  #@type heap::{:node, integer(), integer(), heap(), heap()} | :nil
  #def insert(e, :nil) do {:node, e, 1, :nil, :nil} end
  #def insert(e, {:node, v, c, {:node, lv, _, _, _} = left, {:node, rv, _, _, _} = right}) do
  #  cond do
  #    lcount < rcount ->
  #      {:node, e, c + 1, insert(v, left), right}
  #    true ->
  #      {:node, v, c + 1, left, insert(e, right)}
  #  end
  #end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def traverse(:nil) do [] end
  def traverse({:node, val, left, right}) do [val | traverse(left)++traverse(right)] end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def transf(_, _, []) do [] end
  def transf(x, y, [x|t]) do transf(x, y, t) end
  def transf(x, y, [h|t]) do [h*y | transf(x, y, t)] end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def tailSum(l) do tailSum(l, 0) end
  def tailSum([], res) do res end
  def tailSum([h|t], res) do tailSum(t, h + res) end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def minTree(tree) do minTree(tree, :inf) end
  def minTree(:nil, m) do m end
  def minTree({:node, v, left, right}, m) do
    cond do
      v < m ->
        minTree(left, minTree(right, v))
      true ->
        minTree(left, minTree(right, m))
    end
  end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def start(n) do spawn(fn -> sum(n) end) end
  def sum(n) do
    receive do
      {:add, s} ->
        sum(n + s)
      {:sub, s} ->
        sum(n - s)
       {:req, pid} ->
         send(:total, n)
         sum(n)
    end
  end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##Stack implementation
  def push(e, []) do [e] end
  def push(e, l) do [e|l] end
  def pop([]) do {:fail, []} end
  def pop([e|t]) do {:ok, e, t} end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##FIFO-queue implementation
  @type queue::{:queue, :list, :list} | :nil
  def enqueue(e) do {:queue, [], [e]} end
  def enqueue(e, {:queue, first, last}) do {:queue, first, [e|last]} end
  def dequeue({:queue, [], []}) do :error end
  def dequeue(:nil) do :error end
  def dequeue({:queue, [e|r], t}) do {:ok, e, {:queue, r, t}} end
  def dequeue({:queue, [], t}) do dequeue({:queue, reverse(t), []}) end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def facl(n) do facl(n, 2, [1|[]]) end
  def facl(n, n, [h|t] = l) do [n*h|l] end
  def facl(n, m, [h|t] = l) do facl(n, m + 1, [m*h|l]) end
end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
defmodule Huffman do
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def sample() do
    'This is a sample text'
  end
  def freq(sample) do freq(sample, []) end
  def freq([], freq) do freq end
  def freq([c|rest], freq) do freq(rest, freqc(c, freq)) end
  def freqc(c, []) do [{:freq, c, 1}] end
  def freqc(c, [{:freq, c, n}|rest]) do [{:freq, c, n + 1}|rest] end
  def freqc(c, [h|r]) do [h|freqc(c, r)] end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
defmodule Bitonic do
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  def comp(low, high) do spawn(fn() -> comp(0, low, high) end) end
  def comp(n, low, high) do
    receive do
      {:done, ^n} ->
        ##Terminate both processes by sending 'done' message
        send(low, {:done, n})
        send(high, {:done, n})
      {:epoc, ^n, x1} ->
        receive do
          {:epoc, ^n, x2} ->
            cond do
              x1 < x2 ->
                send(low, {:epoc, n, x1})
                send(high, {:epoc, n, x2})
              true ->
                send(low, {:epoc, n, x2})
                send(high, {:epoc, n, x1})
            end
            comp(n + 1, low, high)
        end
    end
  end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
defmodule Cell do
  def new() do spawn_link(fn -> cell(:open) end) end
  defp cell(state) do
    receive do
      {:get, from} ->
        send(from, {:ok, state})
        cell(state)
      {:set, value, from} ->
        send(from, :ok)
        cell(value)
    end
  end
  def get(cell) do
    send(cell, {:get, self()})
    receive do
      {:ok, value} ->
        value
    end
  end
  def set(cell, value) do
    send(cell, {:set, value, self()})
    receive do
      :ok ->
        :ok
    end
  end
end
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
