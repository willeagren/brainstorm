defmodule Fac do
  def fac(1) do
    1
  end
  def fac(n) do
    n*fac(n-1)
  end

  def facli(1) do
    [1]
  end
  def facli(n) do
    [fac(n)|facli(n-1)]
  end

  def facl(n) do
    facl(n, 2, [1|[]])
  end
  def facl(n, n, [h|t]) do
    [n*h|t]
  end
  def facl(n, m, [h|t] = l) do
    facl(n, m + 1, [m*h|l])
  end
end
