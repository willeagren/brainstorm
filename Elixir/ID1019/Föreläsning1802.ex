#Asynkront vs Synkront
#The memory should be a zero indexed mutable data structure of a given size and provide the following functions:

defmodule Mem do
  def new(l) do
    #memes FeelsGoodMan
    {:mem, List.to_tuple(l)}
  end
  def read({:mem, mem}, n) do
    elem(mem, n)
  end
  def write({:mem, mem}, n, val) do
    {:mem, put_elem(mem, n, val)}
  end
  def test() do
    #Två versioner av mitt minne
    mem1 = Mem.new([:a, :b, :c, :d, :e, :f])
    mem2 = Mem.write(mem1, 3, :foo)
    take_a_look_at_this(mem1)
    and_check_this(mem2)
  end
  def test() do
    #Om vi istället gör såhär?
    mem = Mem.new([:a, :b, :c, :d, :e, :f])
    mem = Mem.write(mem, 3, :foo)
    take_a_look_at_this(mem)
    and_check_this(mem)
  end
  #Can we cheat, and introduce a mutable data structure? Yes, but maybe not worth it
  #Can we use processes to implement mutable data structures?
  def cell(v) do
    receive do
      {:read, pid} ->
        send(pid, {:value, v})
        cell(v)
      {:write, w, pid} ->
        send(pid, :ok)
        cell(w)
      :quit ->
        :ok
    end
  end
#-------------------------------------------------------------------------------
  #A Synchronus interface
  def read({:cell, cell}) do
    send(cell, {:read, self()})
    receive do
      {:value, v} ->
        v
    end
  end
  def write({:cell, cell}, value) do
    send(cell, {:write, val, self()})
    receive do
      :ok ->
        :ok
    end
  end
#-------------------------------------------------------------------------------
#The evil of mutability
this = check_this(mem)
%%'I hope it did not change anything'
that = check_that(mem)
end
