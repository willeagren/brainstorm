#Högre ordningens funktioner
#closure, inneslutning
#Lexikaliskt scope,
defmodule Higherorder do
  @type suit :: :spade | :heart | :diamond | :club
  @type value :: 2..14
  @type card :: {:card, suit, value}
  @spec sort([card]) :: [card]
  @spec split([card]) :: {[card], [card]}
  @spec merge([card], [card]) :: [card]

  def foldr([], acc, op) do
    acc
  end
  def foldr([h | t], acc, op) do
    op.(h, foldr(t, acc, op))
  end
  def sum(l) do
    add = ...
    foldr(l, ..., add)
  end
  def merge([], s2) do
    s2
  end
  def merge(s1, [])do
    s1
  end
  def merge([c1 | r1] = s1, [c2 | r2] = s2) do
    case lt(c1, c2) do
      true ->
        [c1 | merge(r1, s2)]
      false ->
        [c2, merge(s1, r2)]
    end
  end
  def split([]) do
    {[], []}
  end
  def split([c | rest]) do
    {s1, s2} = split(rest)
    {s2, [c | s1]}
  end
  def sort([]) do
    []
  end
  def sort([h|[]]) do
    h
  end
  def sort(deck) do
    {d1, d2} = split(deck)
    merge(sort(d1), sort(d2))
  end
  def lt({:card, s, v1}, {:card, s, v2}) do
    v1 < v2
  end
  def lt({:card, :club, _}, _) do
    true
  end
  def lt({:card, :diamond, _}, {:card, :heart, _}) do
    true
  end
  def lt({:card, :diamond, _}, {:card, :spade, _}) do
    true
  end
  def lt({:card, :heart, _}, {:card, :spade, _}) do
    true
  end
  def lt({:card, _, _}, {:card, _, _}) do
    false
  end
end
