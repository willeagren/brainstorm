defmodule M do
  #Function to find the nth element in a list
  def nth(n, l) do
    cond do
      l ==  [] ->
        []
      n == 0 ->
        hd(l)
      true ->
        nth(n - 1, tl(l))
    end
  end
  #Function to find the length of a list
  def len(l) do
    len(l, 0)
  end
  #Function used in len(l)
  def len(l, n) do
    cond do
      tl(l) == [] ->
        n + 1
      true ->
        len(tl(l), n + 1)
    end
  end
  #Function to find the sum of all elements in a list
  def sum(l) do
    cond do
      tl(l) == [] ->
        hd(l)
      true ->
        hd(l) + sum(tl(l))
    end
  end
  #Function to duplicate a list
  def duplicate(l) do
    cond do
      tl(l) == [] ->
        [hd(l),hd(l)]
      true ->
        [hd(l) | [hd(l)| duplicate(tl(l))]]
    end
  end
  #Function to add an element to a list, if the element was not already present in the list
  def add(x, l) do
    cond do
      contains(x, l) ->
        IO.puts("Your number already exists in the list")
      true ->
        [x | l]
    end
  end
  #Function to remove and element from a list, if the element is present in the list
  def remove(x, l) do
    cond do
      hd(l) != x && tl(l) == [] ->
        [hd(l)]
      hd(l) == x ->
        remove(x, tl(l))
      true ->
        [hd(l) | remove(x, tl(l))]
    end
  end
  #Function that returns a list where the order of elements is reversed
  def reverse(l) do
    [h | t] = l
    reverse(t, [h])
  end
  def reverse([], u) do
    u
  end
  def reverse(l, u) do
    [h | t] = l
    reverse(t, [h | u])
  end
  #Function that returns a list containing lists of equal element
  def pack(l) do
    u = unique(l)
    pack(l, u)
  end
  def pack(l, u) do
    [h | t] = l
    cond do
      tl(u) == [] ->
        [h]
      h == hd(u) ->
        [h | pack(t)]
      true ->
        pack(t)
    end
  end
  #Function that returns a list of unique elements in the list l
  def unique(l) do
    [h | t] = l
    cond do
      t == [] ->
        [h]
      !contains(h, t) ->
        [h | unique(t)]
      true ->
        unique(t)
    end
  end
  #Function to check if a list contains a certain element
  def contains(x,l) do
    cond do
      hd(l) == x ->
        true
      tl(l) == [] && hd(l) != x ->
        false
      true ->
        contains(x,tl(l))
    end
  end
  def insert(e, l) do
    cond do
      #baseCase
      l == [] ->
        [e]
      #baseCase
      tl(l) == [] && e > hd(l) ->
        [hd(l), e]
      #baseCase
      tl(l) == [] && e < hd(l) ->
        [e, hd(l)]
      e <= hd(l) ->
        [e | l]
      true ->
        [hd(l) | insert(e, tl(l))]
    end
  end
  def isort(l) do
    isort(l, [])
  end
  def isort(l, s) do
    cond do
      l == [] ->
        s
      true ->
        isort(tl(l), insert(hd(l), s))
    end
  end
  #Mergesort
  def mergesort(l) do
    case l do
      #If the list is empty
      [] ->
        []
      #If there is only one element in the list
      [h|[]] ->
        [h]
      #true->
      [h|_] ->
        {left, right} = msplit(l,[],[])
        #Recursive call to sort
        merge(mergesort(left), mergesort(right))
    end
  end
  #Case when one side is completely sorted
  #and the other is an empty list
  def merge([],right) do
    right
  end
  #Same as above function
  def merge(left, []) do
    left
  end
  #Sort the two splitted lists
  def merge(left, right) do
    [h1 | t1] = left
    [h2 | t2] = right
    cond do
      h1 < h2 ->
        [h1 | merge(t1, right)]
      true ->
        [h2 | merge(left, t2)]
    end
  end
  #Bascase for splitting the lists,
  #when the list argument is only one element
  def msplit([h|[]], left, right) do
    msplit([], [h|left], right)
  end
  #Split the lists
  def msplit(l, left, right) do
    case l do
      [] ->
        {left, right}
      [h1, h2 | t] ->
        msplit(t, [h1| left], [h2| right])
    end
  end
  #Quicksort
  def quick([]) do
    []
  end
  def quick([pivot | rest]) do
    {left, right} = Enum.partition(rest, &(&1 < pivot))
    quick(left) ++ [pivot | quick(right)]
  end
  #Function to convert Integer to Binary
  def bin(n) do
    bin(n, [])
  end
  def bin(n, l) do
    cond do
      n == 0 ->
        l
      true ->
        bin(div(n, 2), [rem(n, 2) | l])
    end
  end
  #Function to find the fibonacci value for a number n
  def fib(n) do
    cond do
      n == 0 ->
        0
      n == 1 ->
        1
      true ->
        fib(n-1) + fib(n-2)
    end
  end
  #Function to convert Binary to Integer
  def int(l) do
    [h | t] = l
    cond do
      t == [] ->
        cond do
          h == 1 ->
            1
          true ->
            0
        end
      h == 1 ->
        exp(2, len(l) - 1) + int(t)
      true ->
        int(t)
    end
  end
  #Function to multiply
  def product(m,n) do
    cond do
      m == 0 ->
        0
      true ->
        n + product(m - 1, n)
    end
  end
  #Function to take the exponent
  def exp(x, n) do
    cond do
      n == 0 ->
        1
      n == 1 ->
        x
      true ->
        product(x, exp(x, n - 1))
    end
  end
end
