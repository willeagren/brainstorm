#When defining our mandelbrot set we need to be able to work with complex numbers.
#We want to be able to create a tuple as a complex number with a real part and one imaginary part.
#We need the ability to: add sets of complex numbers together, get the aboslute value of a complex term and squaring a complex term.
#
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
defmodule Cmplx do

  #Returns the complex number with real value r and imaginary i.
  def new(r, i) do
    #Represent the complex numbers as a tuple with real value as left element,
    #and imaginary value as left element.
    {r, i}
  end

  #Returns the sum of two complex numbers as a tuple.
  def add({a, b}, {c, d}) do
    #Add real part with real, and imaginary part with imaginary.
    {a + c, b + d}
  end

  #Returns the square of a complex number
  def sqr({r, i}) do
    #(r + i)^2 = r² - i² + 2ri
    {(r*r - i*i), (2*r*i)}
  end

  #Returns the absolute value of a
  def abs({r, i}) do
    #|(r + i)| = sqrt(r² + i²)
    :math.sqrt(r*r + i*i)
  end
end
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#This is the main module for generating our mandelbrot set
#It will implement the computation of the 'i' value given a complex 'c'.
#We will ofcourse give it a maximum iteration 'depth' or we risk getting stuck in an infinite computation.
defmodule Brot do

  #Given the complex number 'c' and the maximum number of iterations m,
  #return the value 'i' at which |zi| > 2 or 0 if it does not for any i < m.
  #That is, it returns a value in the range 0..(m - 1).
  def mandelbrot({r, i}, m) do
    z0 = Cmplx.new(0, 0)
    test(0, z0, {r, i}, m)
  end

  #Test if we hve reached the maximum number of iterations.
  def test(i, z, c, m) do
    cond do
      #Maximum number of iterations reached, return 0.
      i >= m ->
        0
      #Absolute value of z is greater than 2, return 'i'.
      Cmplx.abs(z) > 2 ->
        i
      #Recursively call test/4, increment counter 'i' & go to 'n+1' -> zn+1 = zn² + c
      true ->
        test(i + 1, Cmplx.add(Cmplx.sqr(z), c), c, m)
    end
  end
end

defmodule Color do

end
