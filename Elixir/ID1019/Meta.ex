# This is code for the Meta-interpreter developed for the second seminar in the course Programmering2 ID1019
# Written by: Wilhelm Oscar Mattias Ågren
# Email: wagren@kth.se
# Last modified date: 04/02-2019
#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#The MIT License (MIT)
#
#Copyright (c) <year> <copyright holders>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#
# There is a difference between 'terms', 'patterns' and 'data structures'.
# Given the following sequence: x = :foo; y = :nil, {z, _} = {:bar, :grk}; {x, {z, y}}
# The following constructs are 'terms': :foo, :nil, {:bar, :grk} and {x, {z, y}}
# Terms are syntactical constructs that 'when evaluated' will result in 'data structure'.
# The patterns in the sequence are: x, y and {z, _}.
# If we 'evaluate' the sequence, we gradually build an environment.
# So that the final expression is evaluated in the environment: {x/foo, y/nil, z/bar}.
# In this environment, the term {x, {z, y}} is evaluated to the data structure {foo, {bar, nil}}.
#
# pattern = term; pattern = term; pattern = term; /The pattern matching evaluation?
# How to define an environment? maybe: a list with tuples, where the tuples contain a variable and its bound structure?
# [:env, {id, str}] ? or even just [{id, str}]? In that case: env = [{id1, str1}, {id2, str2}, ..., {idj, strj}] where j is an arbitrary whole number?
defmodule Env do

  #Returns a new environment, thus there are no 'variables' bound to any 'data structures'.
  def new() do
    []
  end

  #Returns a modified environment where the variable 'id' has been bound to the structure 'str'.
  def add(id, str, []) do
    [{id, str}]
  end
  def add(id, str, env) do
    cond do
      #If the variable does not already have a binding in the environment, add the binding to the environment
      lookup(id, env) == :nil ->
        [{id, str} | env]
      true ->
        {v, r} = lookup(id, env)
        cond do
          #If the variable is already bound to the structure, return the environment
          {v, r} == {id, str} ->
            env
          #If the variable already exists in the environment, but is bound to another structure ->
          #Remove the binding and then add the new binding to the environment
          v == id && r != str ->
            [{id, str} | remove([id], env)]
        end
    end
  end

  #Returns either: {id, str} if the variable was bound, or :nil.
  #Base case when the environment is empty, return :nil
  def lookup(_, []) do
    :nil
  end
  #Recursively look through the environment to see if there are any structures bound to the variable id.
  #If there is a binding to the variable, display it, '{variable, structure}'.
  def lookup(id, [l | rest]) do
    #Patternmatch the first binding in the environment.
    {v, str} = l
    cond do
      #If our variable 'id' is equal to the variable 'v' in the binding {v, str}.
      v == id ->
        #Show the variable and struture binding.
        {id, str}
      true ->
        #Recursively look through the entire environment for a binding.
        lookup(id, rest)
    end
  end

  #Returns an environment where all bindings to the variables in the list 'ids' have been removed.
  #Clause to catch if the argument environment is empty, i.e. it contains no bindings.
  def remove(_, []) do
    []
  end
  #[h | tail] is a list of variables that we want to remove, env is the environment list containing all bindings.
  def remove([h | tail], env) do
    remove(tail, remove(h,env))
  end
  #id is the variable we want removed from the environment, env is the environment list containing all bindings.
  def remove(id, [l | rest]) do
    #Patternmatch the first binding in the environment.
    {v, s} = l
    cond do
      #If the variable 'id' is equal to the variable 'v' in the binding.
      id == v ->
        #Recursively call the function without adding the binding back to the environment again
        remove(id, rest)
      true ->
        #Rebuild the environment
        [{v, s} | remove(id, rest)]
    end
  end
end

#Evaluating expressions
defmodule Eager do

  #Evaluating an atom always yields its respective structure no matter which environment.
  def eval_expr({:atm, id}, env) do
    {:ok, id}
  end
  #Evaluating a variable 'id' in the environment 'env'.
  def eval_expr({:var, id}, env) do
    #Case if the variable is part of a binding in the environment.
    case Env.lookup(id, env) do
      #Not part of a binding, return 'error'.
      :nil ->
        :error
      #Part of a binding, return the structure 'str' bound to the variable 'id'.
      {_, str} ->
        {:ok, str}
    end
  end
  #Evaluating a construction of tuples in an environment 'env'.
  def eval_expr({:cons, {:var, v}, {:atm, a}}, env) do
    #Case if the atom 'a' is bound to any data structure
    case eval_expr({:atm, a}, env) do
      #Not bound to any data structure, return 'error'.
      :error ->
        :error
      #Bound to a data structure, but we don't care which one.
      {:ok, _} ->
        #Case if the variable 'v' is bound to any data structure in our environment.
        case eval_expr({:var, v}, env) do
          #Not bound to any data structure, return 'error'.
          :error ->
            :error
          #Bound to a data structure, return a tuple with the evaluated structure bound
          #to the variable, and the structure bound to the atom.
          {:ok, str} ->
            {str, a}
        end
    end
  end

  #Patternmatching nothing with something we dont care about yields the same environment.
  def eval_match(:ignore, _, env) do
    {:ok, env}
  end
  #Pattermatching an atom with its corresponding structure yields the same environment.
  def eval_match({:atm, id}, id, env) do
    {:ok, env}
  end
  #Patternmatching an atom with the wrong data structure results in a 'fail'.
  def eval_match({:atm, id}, _, env) do
    :fail
  end
  #Patternmatching a variable 'id' with a data structure 'str' in the environment 'env'.
  def eval_match({:var, id}, str, env) do
    #Case if the variable 'id' is bound to any data structure in the environment 'env'.
    case Env.lookup(id, env) do
      #Variable 'id' is not bound to any data structure in the environment 'env', return 'ok' and a updated environment.
      :nil ->
        {:ok, Env.add(id, str, env)}
      #Variable 'id' is bound to the data structure that we want to match it with, return the same environment.
      {_, ^str} ->
        {:ok, env}
      #Variable 'id' is already bound to some other data structure in the environment 'env'. Return a 'fail'.
      {_, _} ->
        :fail
    end
  end
  #Patternmatch two 'cons' with eachother.
  def eval_match({:cons, hl, tl}, {:cons, hr, tr}, env) do
    #Case if the cons expression evalues to something
    case eval_match(hl, hr, env) do
      #Cons expression returns a 'fail', return 'fail'.
      :fail ->
        :fail
      #Evaluating the first cons expression yield an 'ok' and an updated
      #environment 'updatedEnv', recursively evaluate next cons expression in the updated environment.
      {:ok, updatedEnv} ->
        eval_match(tl, tr, updatedEnv)
    end
  end
  #If we can not match the pattern to the data structure we return 'fail'.
  def eval_match(_, _, _) do
    :fail
  end

  #Evaluate a sequence
  def eval(seq) do
    eval_seq(seq, [])
  end
  def eval_seq([exp], env) do
    eval_expr(exp, env)
  end
  def eval_seq([{:match, l, r} | rest], env) do
    case eval_expr(r, env) do
      :error ->
        :error
      {:ok, str} ->
        vars = extract_vars([{:match, l, r} | rest])
        case eval_match(l, str, Env.remove(vars, env)) do
          :fail ->
            :error
          {:ok, updatedEnv} ->
            eval_seq(rest, updatedEnv)
        end
    end
  end
  #Returns a list of all variables in the pattern.
  def extract_vars([{:match, l, r} | rest]) do
    #I want to plocka ut alla variabler från sequence
  end
  
end
