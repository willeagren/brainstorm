#---------------------------------------------------------------------------------------------------------------------------------------------------------------
#Code writte by Wilhelm Ågren, for course ID1019
#Last updated 19/02-2019
#---------------------------------------------------------------------------------------------------------------------------------------------------------------
defmodule Chopstick do

  def start() do
    ## We link to the dinner process inorder to avoid zombie chopsticks
    stick = spawn_link(fn -> init() end)
    {:stick, stick}
  end

  def start(node) do
    ## We link to the dinner process inorder to avoid zombie chopsticks
    stick = Node.spawn_link(node, fn -> init() end)
    {:stick, stick}
  end

  # The synchronous version of requesting a chopstick.
  def request({:stick, pid}) do
    send(pid, {:request, self()})

    receive do
      :granted -> :ok
    end
  end

  def return({:stick, pid}) do
    send(pid, :return)
  end


  # Using a timeout to detect deadlock, does it work?
  def request({:stick, pid}, timeout) do
    send(pid, {:request, self()})

    receive do
      :granted ->
        :ok
    after
      timeout ->
        :no
    end
  end

  # The better version, we keep track of requests.
  def request({:stick, pid}, ref, timeout) do
    send(pid, {:request, ref, self()})
    wait(ref, timeout)
  end

  defp wait(ref, timeout) do
    receive do
      {:granted, ^ref} ->
        :ok

      {:granted, _} ->
	## this is an old message that we must ignore
        wait(ref, timeout)
    after
      timeout ->
        :no
    end
  end

  # Return a ref taged stick
  def return({:stick, pid}, ref) do
    send(pid, {:return, ref})
  end


  # A asynchronous request, divided into sending the
  # request and waiting for the reply.
  def asynch({:stick, pid}, ref) do
    send(pid, {:request, ref, self()})
  end

  # Don't throw anything away (since there are no old messages)
  def synch(ref) do
    receive do
      {:granted, ^ref} ->
        :ok
    end
  end

  # To terminate the process.
  def quit({:stick, pid}) do
    send(pid, :quit)
  end

  # Initalizing the chopstick.
  defp init(), do: available()

  # The two states of the chopstick.
  defp available() do
    receive do
      {:request, from} ->
        send(from, :granted)
        gone()

      {:request, ref, from} ->
	## only used when we use refs
        send(from, {:granted, ref})
        gone(ref)

      :quit ->
        :ok
    end
  end

  defp gone() do
    receive do
      :return ->
        available()

      :quit ->
        :ok
    end
  end

  defp gone(ref) do
    receive do
      {:return, ^ref}->
	available()

      :quit ->
        :ok
    end
end
end
#---------------------------------------------------------------------------------------------------------------------------------------------------------------
defmodule Philosopher do

  @dream 1000
  @eat 50
  @delay 200

  # Create a new philosopher process.
  def start(hunger, strength, left, right, name, ctrl, seed) do
    spawn_link(fn -> init(hunger, strength, left, right, name, ctrl, seed) end)
  end

  defp init(hunger, strength, left, right, name, ctrl, seed) do
    gui = Gui.start(name)
    :rand.seed(:exsplus, {seed, seed, seed})
    dreaming(hunger, strength, left, right, name, ctrl, gui)
  end

  # Philosopher is in a dreaming state.
  defp dreaming(0, strength, _left, _right, name, ctrl, gui) do
    IO.puts("#{name} is happy, strength is still #{strength}!")
    send(gui, :stop)
    send(ctrl, :done)
  end
  defp dreaming(hunger, 0, _left, _right, name, ctrl, gui) do
    IO.puts("#{name} is starved to death, hunger is down to #{hunger}!")
    send(gui, :stop)
    send(ctrl, :done)
  end
  defp dreaming(hunger, strength, left, right, name, ctrl, gui) do
    IO.puts("#{name} is dreaming...")
    delay(@dream)
    waiting(hunger, strength, left, right, name, ctrl, gui)
  end

  # Philosopher is waiting for chopsticks.
  defp waiting(hunger, strength, left, right, name, ctrl, gui) do
    send(gui, :waiting)
    IO.puts("#{name} is waiting, #{hunger} to go!")

    case Chopstick.request(left) do
      :ok ->
        delay(@delay)

        case Chopstick.request(right) do
          :ok ->
            IO.puts("#{name} received both sticks!")
            eating(hunger, strength, left, right, name, ctrl, gui)
        end
    end
  end

  # Philosopher is eating.
  defp eating(hunger, strength, left, right, name, ctrl, gui) do
    send(gui, :enter)
    IO.puts("#{name} is eating...")

    delay(@eat)

    Chopstick.return(left)
    Chopstick.return(right)

    send(gui, :leave)
    dreaming(hunger - 1, strength, left, right, name, ctrl, gui)
  end

  defp delay(t), do: sleep(t)

  defp sleep(0), do: :ok
defp sleep(t), do: :timer.sleep(:rand.uniform(t))
end
#---------------------------------------------------------------------------------------------------------------------------------------------------------------
defmodule Dinner do
  # Start a dinner.
  def start(n) do
    seed = 1234
    dinner = spawn(fn -> init(n, seed) end)
    Process.register(dinner, :dinner)
  end

  # Start table at another node
  def start(n, node) do
    seed = 1234
    dinner = spawn(fn -> init(n, seed, node) end)
    Process.register(dinner, :dinner)
  end

  # Stop the dinner.
  def stop() do
    case Process.whereis(:dinner) do
      nil ->
	:ok
      pid ->
	send(pid, :abort)
    end
  end

  defp init(n, seed) do
    c1 = Chopstick.start()
    c2 = Chopstick.start()
    c3 = Chopstick.start()
    c4 = Chopstick.start()
    c5 = Chopstick.start()
    ctrl = self()
    Philosopher.start(n, 5, c1, c2, "Arendt", ctrl, seed + 1)
    Philosopher.start(n, 5, c2, c3, "Hypatia", ctrl, seed + 2)
    Philosopher.start(n, 5, c3, c4, "Simone", ctrl, seed + 3)
    Philosopher.start(n, 5, c4, c5, "Elisabeth", ctrl, seed + 4)
    Philosopher.start(n, 5, c5, c1, "Ayn", ctrl, seed + 5)
    wait(5, [c1, c2, c3, c4, c5])
  end

  defp init(n, seed, node) do
    c1 = Chopstick.start(node)
    c2 = Chopstick.start(node)
    c3 = Chopstick.start(node)
    c4 = Chopstick.start(node)
    c5 = Chopstick.start(node)
    ctrl = self()
    Philosopher.start(n, 5, c1, c2, "Arendt", ctrl, seed + 1)
    Philosopher.start(n, 5, c2, c3, "Hypatia", ctrl, seed + 2)
    Philosopher.start(n, 5, c3, c4, "Simone", ctrl, seed + 3)
    Philosopher.start(n, 5, c4, c5, "Elisabeth", ctrl, seed + 4)
    Philosopher.start(n, 5, c5, c1, "Ayn", ctrl, seed + 5)
    wait(5, [c1, c2, c3, c4, c5])
  end


  defp wait(0, chopsticks) do
    Enum.each(chopsticks, fn(c) -> Chopstick.quit(c) end)
  end

  defp wait(n, chopsticks) do
    receive do
      :done ->
        wait(n - 1, chopsticks)

      :abort ->
	## in order to kill all chopsticks and philosophers
        Process.exit(self(), :kill)
    end
end
end
#---------------------------------------------------------------------------------------------------------------------------------------------------------------
defmodule Gui do

  @red {200, 0, 0}
  @yellow {255,255,0}
  @blue {0, 0, 200}
  @black {0, 0, 0}
  @white {1, 1, 1}



  def start(name) do
    spawn_link(fn() -> init(name) end)
  end

  def init(name) do
    width = 400
    height = 400
    server = :wx.new()  #Server will be the parent for the Frame
    frame = :wxFrame.new(server, -1, name, [{:size,{width, height}}])
    :wxFrame.setBackgroundColour(frame, @white)
    :wxFrame.show(frame)
    loop(frame)
  end


  def loop(frame) do
    receive do
      :waiting ->
	:wxFrame.setBackgroundColour(frame, @yellow)
	loop(frame)
      :enter ->
	:wxFrame.setBackgroundColour(frame, @red)
	loop(frame)
      :leave ->
	:wxFrame.setBackgroundColour(frame, @blue)
	loop(frame)
      :abort ->
	:wxFrame.setBackgroundColour(frame, @black)
	loop(frame)
      :stop ->
	:ok
      error ->
	:io.format("gui: strange message ~w ~n", [error])
	loop(frame)
    end
  end

end
