defmodule M do
  def double(n) do
    n*2
  end
  def product(m,n) do
    cond do
      m == 0 ->
        0
      true ->
        n + product(m - 1, n)
    end
  end
  def exp(x, n) do
    cond do
      n == 0 ->
        1
      n == 1 ->
        x
      true ->
        product(x, exp(x, n - 1))
    end
  end
  def expSUPASPEED(x, n) do
    cond do
      n == 0 ->
        1
      n == 1 ->
        x
      rem(n, 2) == 1 ->
        product(x, exp(x, n - 1))
      true ->
        product(exp(x, div(n,2)),exp(x, div(n,2)))
    end
  end
end
