#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
#Code written by Wilhelm Ågren, last updated: 26-02-2020
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
defmodule Rudy do

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
  def start(port) do
    Process.register(spawn(fn -> init(port) end), :rudy)
  end
  def stop() do
    case Process.whereis(:rudy) do
      nil ->
        :ok
      pid ->
        Process.exit(pid, "Time to die!")
    end
  end
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
  ##This function will initalize the server, takes a port number, opens a
  ##listening socket and passes the socket to 'handler/1'. Once the request
  ##has been handled the socket will be closed.
  def init(port) do
    opt = [:list, active: false, reuseaddr: true]
    case :gen_tcp.listen(port, opt) do
      ###Pass the listening socket to 'handler/1'
      {:ok, listen} ->
        handler(listen)
        :gen_tcp.close(listen)
        :ok
      {:error, error} ->
        error
    end
  end

  ##This function will listen to the socket for an incoming connection.
  ##Once a client has connected it will pass the connection to 'request/1'.
  ##When the request is handled the connection will be closed.
  def handler(listen) do
    case :gen_tcp.accept(listen) do
      ###Pass the client connection to 'request/1'.
      {:ok, client} ->
        request(client)
        :gen_tcp.close(client)
        ###Recursively call handler to keep the server up
        ###so it doesn't close on the first client.
        handler(listen)
      {:error, error} ->
        error
    end
  end

  ##This function will read the request from the client connection and parse it.
  ##It will then parse the request using our 'HTTP-parser' and pass the
  ##request to 'reply/1'. The reply is then sent back to the client.
  def request(client) do
    ###Jag vill ha allt du har i den här bufferten '0'.
    recv = :gen_tcp.recv(client, 0)
    case recv do
      {:ok, str} ->
        request = HTTP.parse_request(str)
        response = reply(request)
        :gen_tcp.send(client, response)
      {:error, error} ->
        IO.puts("RUDY ERROR: #{error}")
    end
    :gen_tcp.close(client)
  end

  ##This function is where we decide on what to reply, most importantly,
  ##how to turn the reply into a neatly and well formated HTTP reply.
  def reply({{:get, uri, _}, _, _}) do
    :timer.sleep(10)
    HTTP.ok("Welcome! Now please leave :'-)")
  end
end
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
defmodule HTTP do

  ##We implement each parsing function so that it will parse its element and
  ##return a tuple consisting of the parsed result and the rest of the string.
  def parse_request(r0) do
    {request, r1} = request_line(r0)
    {headers, r2} = headers(r1)
    {body, _} = message_body(r2)
    {request, headers, body}
  end

  ##The request line consists of: a 'method', a request 'URI' and a 'HTTPversion'.
  ##String are represented as lists with integers, the integers corresponding to
  ##the correct ASCII value of the character.
  def request_line([?G, ?E, ?T, 32 | r0]) do
    {uri, r1} = request_uri(r0)
    {ver, r2} = http_version(r1)
    [13, 10 | r3] = r2
    {{:get, uri, ver}, r3}
  end

  ##The 'URI' is returned as a string.
  ##Recursive base case if we have traversed the entire 'URI'.
  def request_uri([32 | r0]) do
    {[], r0}
  end
  def request_uri([c | r0]) do
    ###Recursively build the 'URI-string'.
    {rest, r1} = request_uri(r0)
    {[c | rest], r1}
  end

  ##Return 'HTTP/1.1' and the rest of the 'GET' request.
  def http_version([?H, ?T, ?T, ?P, ?/, ?1, ?., ?1 | r0]) do
    {:v11, r0}
  end
  ##Return 'HTTP/1.0' and the rest of the 'GET' request.
  def http_version([?H, ?T, ?T, ?P, ?/, ?1, ?., ?0 | r0]) do
    {:v10, r0}
  end

  ##Headers also have internal structure but we are only interested in dividing
  ##them up into individual string and most importantly find the end of the
  ##header section. We implement this as two recursive functions;
  ##Consumes a sequence of headers.
  def headers([13, 10 | r0]) do
    {[], r0}
  end
  def headers(r0) do
    {header, r1} = header(r0)
    {rest, r2} = headers(r1)
    {[header | rest], r2}
  end

  ##Consumes individual headers.
  def header([13, 10 | r0]) do
    {[], r0}
  end
  def header([c | r0]) do
    {rest, r1} = header(r0)
    {[c | rest], r1}
  end

  ##We assume that all that's left is the body,
  ##which makes parsing the body incredibly easy.
  def message_body(r) do
    {r, []}
  end

  ##Generate a HTTP reply
  def ok(body) do
    "HTTP/1.1 200 OK\r\n\r\n #{body}"
  end
  def get(uri) do
    "GET #{uri} HTTP/1.1\r\n\r\n"
  end
end
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
defmodule Test do

  @number_requests 100

  def bench(host, port) do
    start = Time.utc_now()
    run(@number_requests, host, port)
    finish = Time.utc_now()
    diff = Time.diff(finish, start, :millisecond)
    IO.puts("Benchmark: #{@number_requests} requests in #{diff} ms")
  end

  defp run(0, _host, _port), do: :ok
  defp run(n, host, port) do
    request(host, port)
    run(n - 1, host, port)
  end

  defp request(host, port) do
    opt = [:list, active: false, reuseaddr: true]
    {:ok, server} = :gen_tcp.connect(host, port, opt)
    :gen_tcp.send(server, HTTP.get("foo"))
    {:ok, _reply} = :gen_tcp.recv(server, 0)
    :gen_tcp.close(server)
  end
end
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------