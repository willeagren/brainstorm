defmodule M do
  def insert(e, l) do
    [h | t] = l
    cond do
      #baseCase
      t == [] && e > h ->
        [h, e]
      #baseCase
      t == [] && e < h ->
        [e, h]
      e <= h ->
        [e | l]
      true ->
        [h | insert(e, t)]
    end
  end
  def isort(l) do
    isort(l, len(l))
  end
  def isort(l, n) do
    [h | t] = l
    cond do
      n == 0 ->
        [h]
      true ->
        isort(insert(h,t), n - 1)
    end
  end
end
