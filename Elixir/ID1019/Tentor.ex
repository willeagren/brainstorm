defmodule Tenta do
  def listAdd(id, []) do
    [id]
  end
  def listAdd(id, l) do
    [id|l]
  end
  def rlistRemove(_, []) do
    []
  end
  def listRemove(id, [h|t]) do
    cond do
      id == h ->
        listRemove(id,t)
      true ->
        [h|listRemove(id,t)]
    end
  end
#---------------------------------------------------------------------------------------------------------------------------------------------
  #Exempel tentamen: Uppgift 1 - Duplicera alla positiva tal i en lista
  def listDuplicateEven([]) do
    []
  end
  def listDuplicateEven([h | t]) do
    case t do
      [] ->
        case rem(h, 2) do
          0 ->
            [h | [h]]
          _ ->
            [h]
        end
      _ ->
        case rem(h , 2) do
          0 ->
            [h | [h | listDuplicateEven(t)]]
          _ ->
            [h | listDuplicateEven(t)]
        end
    end
  end
#---------------------------------------------------------------------------------------------------------------------------------------------
  #Exempel tentamen: Uppgift 2 - Summera noderna i ett träd
  @type tree :: {:node, integer(), tree(), tree()} | :nil
  def treeSpawn(value) do
    {:node, value, :nil, :nil}
  end

  def treeAdd(value, :nil) do
    treeSpawn(value)
  end
  def treeAdd(value, {:node, treeValue, left, right}) do
    cond do
      value > treeValue ->
        {:node, treeValue, left, treeAdd(value, right)}
      true ->
        {:node, treeValue, treeAdd(value, left), right}
    end
  end

  def treeSum(tree) do
    treeSum(tree, 0)
  end
  def treeSum(:nil, sum) do
    sum
  end
  def treeSum({:node, value, left, right}, sum) do
    value + treeSum(left, sum) + treeSum(right, sum)
  end
#---------------------------------------------------------------------------------------------------------------------------------------------
  #Exempel tentamen: Uppgift 3 - Spegelvänd ett träd
  def treeMirror({:node, value, left ,right}) do
    {:node, value, treeMirror(right), treeMirror(left)}
  end
  def treeMirror(:nil) do
    :nil
  end
#---------------------------------------------------------------------------------------------------------------------------------------------
  #Exempel tentamen: Uppgift 4 - Addera ett tal till en heap
  #@spec add(heap(), integer()) :: heap()
  def heapSpawn(n) do
    {:node, n, :nil, :nil}
  end

  def heapAdd(:nil, n) do
    heapSpawn(n)
  end
  def heapAdd({:node, m, left, right}, n) do
    cond do
      n > m ->
        {:node, n, {:node, m, :nil, left}, right}
      true ->
        {:node, m, left, heapAdd(right, n)}
    end
  end
#---------------------------------------------------------------------------------------------------------------------------------------------
  #Exempel tentamen: Uppgift 5 - Fizz-Buzz
  def fizzbuzz(n) do
    cond do
      n < 1 ->
        "Argument Error: your number can NOT be smaller than 1!"
      true ->
        fizzbuzz(1, n + 1, 1, 1)
    end
  end
  #Första argumentet är nästa element i listan
  #Andra argumentet är för att veta att vi är klara
  #Tredje och fjärde argumentet håller reda på om talet är delbart med 3 och/eller 5
  def fizzbuzz(n, n, _, _) do
    []
  end
  def fizzbuzz(i, n, 3, 5) do
    [:fizzbuzz | fizzbuzz(i + 1, n, 1, 1)]
  end
  def fizzbuzz(i, n, 3, b) do
    [:fizz | fizzbuzz(i + 1, n, 1, b + 1)]
  end
  def fizzbuzz(i, n, a, 5) do
    [:buzz | fizzbuzz(i + 1, n, a + 1, 1)]
  end
  def fizzbuzz(i, n, a, b) do
    [i | fizzbuzz(i + 1, n, a + 1, b + 1)]
  end
#---------------------------------------------------------------------------------------------------------------------------------------------
  def sum({:node, treeValue, left, right}) do
    treeValue + sum(left) + sum(right)
  end
  def sum(:nil) do
    0
  end

  #def once([]) do
  #  {0, 0}
  #end
  #def once([h | t]) do
  #  {s, l} = once(t)
  #  {s + h , l + 1}
  #end
  def once(list) do
    once(list, 0, 0)
  end
  def once([], sum, length) do
    {sum, length}
  end
  def once([h | t], sum, length) do
    once(t, sum + h, length + 1)
  end
  def ack(0, n) do
    n + 1
  end
  def ack(m, 0) when m > 0 do
    ack(m - 1, 1)
  end
  def ack(m, n) when m > 0 and n > 0 do
    ack(m - 1, ack(m, n - 1))
  end
  def eval([res | []]) do
    res
  end
  def eval([x, '-', y | rest]) do
    eval([x - y | rest])
  end
  def eval([x, '+', y | rest]) do
    eval([x + y | rest])
  end
  def calc(pol, x) do
    calc(pol, x, 0)
  end
  def calc([], _, sum) do
    sum
  end
  def calc([n | pol], x, sum) do
    calc(pol, x, sum*x + n)
  end
  def concat(s1,s2) do
    {:string, [s1 | s2]}
  end
  def reduce([h | []]) do
    [h]
  end
  def reduce([h | t]) do
    [n | _] = t
    cond do
      h == n ->
        reduce(t)
      true ->
        [h | reduce(t)]
    end
  end
  def encode([]) do [] end
  def encode([32 | t]) do
    [32 | encode(t)]
  end
  def encode([h | t]) do

  end
  def nth(n, []) do
    :error
  end
  def nth(0, [h, _]) do
    h
  end
  def nth(n, [_ | t]) do
    nth(n - 1, t)
  end
  def reverse(list) do
    reverse(list, [])
  end
  def reverse([], rev) do
    rev
  end
  def reverse([h | t], rev) do
    reverse(t, [h | rev])
  end
  def fibb(0) do
    {0,:nil}
  end
  def fibb(1) do
    {1,0}
  end
  def fibb(n) do
    {a, b} = fibb(n - 1)
    {a + b, a}
  end
  def fib(0) do
    0
  end
  def fib(1) do
    1
  end
  def fib(n) do
    fib(n-1) + fib(n-2)
  end
  #@type tree::{:node, integer(), tree(), tree()} | :nil
  def add(n, :nil) do {:node, n, :nil, :nil} end
  def add(n, {:node, m, left, right}) do
    cond do
      n > m ->
        {:node, m, left, add(n, right)}
      true ->
        {:node, m, add(n, left), right}
    end
  end
  def start() do
    spawn(fn() -> omid() end)
  end
  def omid() do
    receive do
      :hungry ->
        search()
      _ ->
        omid()
    end
  end
  def search() do
    receive do
      :crispyChicken ->
        eating()
      :searching ->
        search()
      _ -> omid()
    end
  end
  def eating() do
    IO.puts("This is soooo good!")
    receive do
      :jummy! ->
        eating()
      :done! ->
        omid()
    end
  end
  def rome(["I", "V" | []]) do
    4
  end
  def rome(["I", "V" | rest]) do
    4 + rome(rest)
  end
  def rome(["I", "X" | []]) do
    9
  end
  def rome(["I", "X" | rest]) do
    9 + rome(rest)
  end
  def rome(["X" | []]) do
    10
  end
  def rome(["X" | rest]) do
    10 + rome(rest)
  end
  def rome(["V" | []]) do
    5
  end
  def rome(["V" | rest]) do
    5 + rome(rest)
  end
  def rome(["I" | []]) do
    1
  end
  def rome(["I" | rest]) do
    1 + rome(rest)
  end
  def eleme(x, []) do
    :false
  end
  def eleme(x, [x | _]) do
    :true
  end
  def eleme(x, [h | t]) do
    eleme(x, t)
  end
  def union(l, []) do
    l
  end
  def union(l, [h | t]) do
    case eleme(h, l) do
      :true ->
        union(l, t)
      :false ->
        [h | union(l, t)]
    end
  end
  def isec(_, []) do
    []
  end
  def isec(l, [h | t]) do
    case eleme(h, l) do
      :true ->
        [h | isec(l, t)]
      :false ->
        isec(l, t)
    end
  end
end
