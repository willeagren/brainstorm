defmodule Tree do
  #Initiate a new tree
  def init(key, value) do
    {:node, key, value, :nil, :nil}
  end

  #Insert a key-value pair into a tree, assuming the three exists
  def insert(key, value, :nil) do
    init(key, value)
  end
  def insert(key, value, {:node, otherKey, otherValue, left, right}) do
    cond do
      key < otherKey ->
        {:node, otherKey, otherValue, insert(key, value, left), right}
      true ->
        {:node, otherKey, otherValue, left, insert(key, value, right)}
    end
  end

  #Modify an elemnt, if it is found in the Tree
  def modify(_, _, :nil) do
    :nil
  end
  def modify(key, value, {:node, key, _, left, right}) do
    {:node, key, value, left, right}
  end
  def modify(key, value, {:node, otherKey, otherValue, left, right}) do
    cond do
      key < otherKey ->
        {:node, otherKey, otherValue, modify(key, value, left), right}
      true ->
        {:node, otherKey, otherValue, left, modify(key, value, right)}
    end
  end

  #Cred till Axel Pettersson^TM
  def minValueNode({:node, key, value, :nil, _}) do
    {key, value}
  end
  def minValueNode({:node, _, _, left, _}) do
    minValueNode(left)
  end
  #Delete the node with the 'Key'
  def delete(key, {:node, key, _, :nil, :nil}) do
    :nil
  end
  def delete(key, {:node, key, _, :nil, right}) do
    right
  end
  def delete(key, {:node, key, _, left, :nil}) do
    left
  end

  #Node with two leafs
  def delete(key, {:node, key, _, left, right}) do
    {k, v} = minValueNode(right)
    {:node, k, v, left, delete(key, right)}
  end
  def delete(key, {:node, otherKey, otherValue, left, right}) do
    cond do
      key < otherKey ->
        {:node, otherKey, otherValue, delete(key, left), right}
      true ->
        {:node, otherKey, otherValue, left, delete(key, right)}
    end
  end

  #Check if the key exists in the Tree
  def member(_, :nil) do
    :no
  end
  def member(key, {:node, key, _, _, _}) do
    :yes
  end
  def member(key, {:node, otherKey, _, left, right}) do
    cond do
      key < otherKey ->
        member(key, left)
      true ->
        member(key, right)
    end
  end

  #Return the value if the key is found in the Tree
  def lookup(_, :nil) do
    :no
  end
  #We found the key, return the associated value
  def lookup(key, {:node, key, value, _, _}) do
    {:value, value}
  end
  def lookup(key, {:node, otherKey, _, left, right}) do
    cond do
      key < otherKey ->
        lookup(key, left)
      true ->
        lookup(key, right)
    end
  end

  #Print function for easier analysis, SHOUTOUT TILL AXEL <3<3<3
  def printTree({:node, key, value, :nil, :nil}) do
    IO.write("{ key: #{key}, val: #{value} }")
  end
  def printTree({:node, key, value, :nil, right}) do
    IO.write("{ key: #{key}, val: #{value}, right -> ")
    printTree(right)
    IO.write('}')
  end
  def printTree({:node, key, value, left, :nil}) do
    IO.write("{ key: #{key}, val: #{value}, left -> ")
    printTree(left)
    IO.write('}')
  end
  def printTree({:node, key, value, left, right}) do
    IO.write("{ key: #{key}, val: #{value}, left -> ")
    printTree(left)
    IO.write(', right -> ')
    printTree(right)
    IO.write('}')
  end

  #Huffman Coding. Seminar 1----------------------------------------------------

  #sample
  def sample do
    'the quick brown fox jumps over the lazy dogthis is a sample text that we will use when we buildup a table we will only handle lower case letters andno punctuation symbols the frequency will of course notrepresent english but it is probably not that far off'
  end

  #The text using during our Huffman testing.
  def text do
    'this is something that we should encode'
  end

  #Unit testing
  def test do
    sample = sample()
    tree = tree(sample)
    encode = encode_table(tree)
    decode = decode_table(tree)
    text = text()
    sequence = encode(text, encode)
    decode(sequence, decode)
  end


  #Mergesort
  def mergesort(l) do
    case l do
      #If the list is empty
      [] ->
        []
      #If there is only one element in the list
      [h|[]] ->
        [h]
      #true->
      [h|_] ->
        {left, right} = msplit(l,[],[])
        #Recursive call to sort
        merge(mergesort(left), mergesort(right))
    end
  end
  #Case when one side is completely sorted
  #and the other is an empty list
  def merge([],right) do
    right
  end
  #Same as above function
  def merge(left, []) do
    left
  end
  #Sort the two splitted lists
  def merge(left, right) do
    [h1 | t1] = left
    [h2 | t2] = right
    cond do
      h1 < h2 ->
        [h1 | merge(t1, right)]
      true ->
        [h2 | merge(left, t2)]
    end
  end
  #Bascase for splitting the lists,
  #when the list argument is only one element
  def msplit([h|[]], left, right) do
    msplit([], [h|left], right)
  end
  #Split the lists
  def msplit(l, left, right) do
    case l do
      [] ->
        {left, right}
      [h1, h2 | t] ->
        msplit(t, [h1| left], [h2| right])
    end
  end

  #Represent the frequencies
  def frequency(sample) do
    frequency(sample, [])
  end
  def frequency([], frequency) do
    frequency
  end
  #Recursively iterate through the given list of characters, 'sample', O(N) time complexity
  def frequency([char | rest], frequency) do
    frequency(remove(char, rest), [{:freq, char, countFreq(char, rest, 1)} | frequency])
  end
  #Recrusively iterate through the given list 'l', O(N) time complexity
  def countFreq(char, l, freq) do
    cond do
      l == [] ->
        freq
      tl(l) == [] && hd(l) == char ->
        countFreq(char, tl(l), freq + 1)
      tl(l) == [] && hd(l) != char ->
        countFreq(char, tl(l), freq)
      char == hd(l) ->
        countFreq(char, tl(l), freq + 1)
      true ->
        countFreq(char, tl(l), freq)
    end
  end

  def tree(sample) do
    tree(mergesort(frequency(sample)), [])
  end

  def tree([_|[]], tree) do
    tree
  end
  def tree([{typ1, e1, n}, {typ2, e2, m} | t], tree) do
    cond do
      n > m ->
        node = {:node, {{typ2, e2, m}, {typ1, e1, n}}, m + n}
        tree(insert(node,t),node)
      true ->
        node = {:node, {{typ1, e1, n}, {typ2, e2, m}}, m + n}
        tree(insert(node,t),node)
    end
  end
  #Create an 'encoding table' containing the mapping from characters to codes
  #given a Huffman tree.
  def encode_table({:node, {left, right}, _}) do
    List.flatten([encode_table(left, [0]), encode_table(right, [1])])
  end
  def encode_table({:freq, char,_}, code) do
    {char,code}
  end
  def encode_table({:node, {left, right}, _}, code) do
    [encode_table(left,[0|code]), encode_table(right,[1|code])]
  end

  #Create a 'decoding table' containing the mapping from codes to characters
  #given a Huffman tree.
  def decode_table(tree) do

  end

  #Encode the text using the mapping in the table, return a sequence of bits.
  def encode(text, table) do
    encode(text, table, [])
  end
  def encode([], _, code) do
    List.flatten(code)
  end
  def encode([h | t], table, code) do
    encode(t, table, [getPath(h, table) | code])
  end
  #Retrieve path, and not destroy the table in the process
  def getPath(h, [{char, path} |tail]) do
    cond do
      h == char ->
        path
      true ->
        getPath(h, tail)
    end
  end

  def decode([], _) do
    []
  end
  def decode(sequence, table) do
    {char, rest} = decode_char(sequence, 1, table)
    [char | decode(rest, table)]
  end
  def decode_char(sequence, n, table) do
    {code, rest} = Enum.split(sequence, n)
    case List.keyfind(table, code, 1) do
      {c, _} ->
        {c, rest}
      :nil ->
        decode_char(sequence, n + 1, table)
    end
  end
  #Find the char
  def getChar(code, [{char, code} | _]) do
    char
  end
  def getChar(code, [{_, _} | tail]) do
    getChar(code, tail)
  end
  def getChar(_, []) do
    false
  end

  #Function to remove and element from a list, if the element is present in the list
  def remove(x, l) do
    cond do
      l == [] ->
        []
      hd(l) == x && tl(l) == [] ->
        []
      hd(l) != x && tl(l) == [] ->
        [hd(l)]
      hd(l) == x ->
        remove(x, tl(l))
      true ->
        [hd(l) | remove(x, tl(l))]
    end
  end
  #Function to check if a list contains a certain element
  def contains(x,l) do
    cond do
      l == [] ->
        false
      hd(l) == x ->
        true
      tl(l) == [] && hd(l) != x ->
        false
      true ->
        contains(x,tl(l))
    end
  end
  #Function to find the length of a list
  def len(l) do
    len(l, 0)
  end
  #Function used in len(l)
  def len(l, n) do
    cond do
      tl(l) == [] ->
        n + 1
      true ->
        len(tl(l), n + 1)
    end
  end
  def insert(e, l) do
    {_,m,n} = e
    case l do
      #baseCase
      [] ->
        [e]
      [{ty,char,m} | t] when m < n ->
        [{ty, char, m} | insert(e,t)]
      [{ty, char, m} | t] when m >= n ->
        [e, {ty, char, m} | t]
    end
  end
end
