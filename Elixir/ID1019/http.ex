#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
#Code written by Wilhelm Ågren, last updated: 23/02-2019
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
defmodule HTTP do

  ##We implement each parsing function so that it will parse its element and
  ##return a tuple consisting of the parsed result and the rest of the string.
  def parse_request(r0) do
    {request, r1} = request_line(r0)
    {headers, r2} = headers(r1)
    {body, _} = message_body(r2)
    {request, headers, body}
  end

  ##The request line consists of: a 'method', a request 'URI' and a 'HTTPversion'.
  ##String are represented as lists with integers, the integers corresponding to
  ##the correct ASCII value of the character.
  def request_line([?G, ?E, ?T, 32 | r0]) do
    {uri, r1} = request_uri(r0)
    {ver, r2} = http_version(r1)
    [13, 10 | r3] = r2
    {{:get, uri, ver}, r3}
  end

  ##The 'URI' is returned as a string.
  ##Recursive base case if we have traversed the entire 'URI'.
  def request_uri([32 | r0]) do
    {[], r0}
  end
  def request_uri([c | r0]) do
    ###Recursively build the 'URI-string'.
    {rest, r1} = request_uri(r0)
    {[c | rest], r1}
  end

  ##Return 'HTTP/1.1' and the rest of the 'GET' request.
  def http_version([?H, ?T, ?T, ?P, ?/, ?1, ?., ?1 | r0]) do
    {:v11, r0}
  end
  ##Return 'HTTP/1.0' and the rest of the 'GET' request.
  def http_version([?H, ?T, ?T, ?P, ?/, ?1, ?., ?0 | r0]) do
    {:v10, r0}
  end

  ##Headers also have internal structure but we are only interested in dividing
  ##them up into individual string and most importantly find the end of the
  ##header section. We implement this as two recursive functions;
  ##Consumes a sequence of headers.
  def headers([13, 10 | r0]) do
    {[], r0}
  end
  def headers(r0) do
    {header, r1} = header(r0)
    {rest, r2} = headers(r1)
    {[header | rest], r2}
  end

  ##Consumes individual headers.
  def header([13, 10 | r0]) do {[], r0} end
  def header([c | r0]) do
    {rest, r1} = header(r0)
    {[c | rest], r1}
  end

  ##We assume that all that's left is the body,
  ##which makes parsing the body incredibly easy.
  def message_body(r) do {r, []} end

  ##Generate a HTTP reply
  def ok(body) do "HTTP/1.1 200 OK\r\n\r\n #{body}" end
  def get(uri) do "GET #{uri} HTTP/1.1\r\n\r\n" end
end
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
