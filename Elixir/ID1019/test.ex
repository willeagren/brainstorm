defmodule Test do
  def double(n) do
    IO.puts("Double your number is: ")
    IO.puts(n*2)
  end
  def converter(n) do
    IO.puts("Your temperature in Fahrenheit converted to Celsius is: ")
    IO.puts((n-32)/1.8)
  end
  #Create a Huffman tree given a sample text.
  def tree(sample) do
    #Sort frequency by lowest frequencies first
    huffman(frequency(sample))
  end
  #Build the huffman tree based on the frequency list
  def huffman(frequency) do
    huffman(frequency, :nil)
  end
  def huffman([], tree) do
    tree
  end
  def huffman(frequency, tree) do
    #Take two lowest frequencies, create a parent node with the two frequencies as children, value of parent node is sum of children frequencies
    cond do
      tree == :nil ->
        [[h1,h2],[h3,h4] | rest] = frequency
        huffman(rest, node(h1 + h3, h1 + h3, {h1,h2}, {h3,h4}))
      true ->
        cond do
          len(frequency) <= 1 ->
            [[h1,h2] | rest] = mergesort(frequency)
            huffman(rest, node(h1 + treeValue(tree), h1 + treeValue(tree), {h1,h2}, tree))
          true ->
            [[h1,h2],[h3,h4] | rest] = mergesort(frequency)
            node = node(h1 + h3, h1 + h3, {h1,h2}, {h3,h4})
            huffman(rest, node(treeValue(node) + treeValue(tree), treeValue(node) + treeValue(tree), node, tree))
        end
    end
    #Take next lowest frequency, create a new parent node with the last parent and new lowest frequency as children, value is sum of children frequencies
    #Continue until frequency = []
  end
  def treeValue({:node, value, _, _,_}) do
    value
  end
  def node(value, key, left, right) do
    {:node, value, key, left, right}
  end
end
