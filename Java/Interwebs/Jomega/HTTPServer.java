import java.io.*;
import java.net.*;
import java.util.*;
public class HTTPServer {

    // HTTP actions
    private static final File WEB_ROOT = new File(".");
    private static final String DEFAULT_FILE = "";
    private static final String FILE_NOT_FOUND = "404.html";

    // Socket port to listen on
    private static final int PORT = 8080;

    // Verbose mode for more information
    private static final boolean verbose = true;
    // Client connects via the socket API
    private Socket client_socket;

    // Constructor for the HTTPserver, sets correct socket
    public HTTPServer(Socket s) {
        this.client_socket = s;
    }

    public static void main(String[] args) throws IOException {
        try{
            ServerSocket server_connect = new ServerSocket(PORT);

            // Listen on socket until user halts server execution
            while(true) {
                HTTPServer server = new HTTPServer(server_connect.accept());
                if(verbose) {
                    System.out.println("Connection opened. [" + new Date() + "]");
                }
                Thread thread = new Thread(server);
                thread.start();
            }
        } catch (IOException e) {
            System.err.println("[ERROR] Server connection error: +" + e.getMessage());
        }
    }

    // Manage the particular client connections here
    @Override
    public void run() {
        BufferedReader in = null;
        PrintWriter out = null;
        BufferedOutputStream data_out = null;
        String request = null;

        try{
            in = new BufferedReader(new InputStreamReader(client_socket.getInputStream()));
        } catch (IOException e) {
            System.err.println("[ERROR] Server error when handling client: " + e.getMessage());
        }
    }
}