#!/bin/bash
cd ..
echo "Compiling dynprog version..."
javac Enum_encoded_dyn.java
echo "Compiling recursive version..."
javac Enum_encoded_rec.java
echo "Done compiling."
echo "Executing..."
NUM=1
for ((i = 1; i < 1000000000000000000000; i))
do
	echo -n "$i" >> Bench/dyn_vs_rec_bench.dat
	java -cp $HOME/Bund/Java/daily_coding/ Enum_encoded_dyn "$i" | tee -a Bench/dyn_vs_rec_bench.dat
	java -cp $HOME/Bund/Java/daily_coding/ Enum_encoded_rec "$i" | tee -a Bench/dyn_vs_rec_bench.dat
	echo -n -e "\n" >> Bench/dyn_vs_rec_bench.dat
	i="$i$NUM"
done
echo -e "\nComputation completed."
