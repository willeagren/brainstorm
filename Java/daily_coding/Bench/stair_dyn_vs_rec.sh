#!/bin/bash
rm stair_dyn_vs_rec.dat
cd ..
echo "--== [Benchmarking] ==--"
echo "Compiling dynprog version..."
javac Staircase_dyn.java
echo "Compiling recursive version..."
javac Staircase_rec.java
echo "Done compiling."
echo "Executing..."
for ((i = 1; i < 40; i++))
do
	echo -n "$i" >> Bench/stair_dyn_vs_rec.dat
	java -cp $HOME/Bund/Java/daily_coding/ Staircase_dyn "$i" "1,2" | tee -a Bench/stair_dyn_vs_rec.dat
	java -cp $HOME/Bund/Java/daily_coding/ Staircase_rec "$i" | tee -a Bench/stair_dyn_vs_rec.dat
	echo -n -e "\n" >> Bench/stair_dyn_vs_rec.dat
done
echo -e "\nExecution completed."
