/**
 *  Daily Coding Problem [7] - Facebook
 * 
 *  Given the mapping a = 1, b = 2, ..., z = 26, 
 *  and an encoded message, coun the number of 
 *  ways it can be decoded. 
 * 
 *  This is a implementation using dynamic programming.
 * 
 *  Author: Wilhelm Ågren, wagren@kth.se
 *  Last modified: 11/03/2020
 **/
public class Enum_encoded_dyn {

    private int enum_code_dynprog(int[] code, int n) {
        int[] dyn = new int[n + 1];
        dyn[0] = 1;
        dyn[1] = 1;
        for(int i = 2; i <= n; i++) {
            dyn[i] = 0;
            if(code[i - 1] > 0) dyn[i] = dyn[i - 1];
            if(code[i - 2] == 1 || code[i - 2] == 2 && code[i - 1] < 7) {
                dyn[i] += dyn[i - 2];
            }
        }
        return dyn[n];
    }

    public static void main(String[] args) {
        String arg = "";
        try {
            arg = args[0];
        } catch (Exception e) {
            System.err.println("[ERROR] Incorrect argument formatting...");
            return;
        }
        Enum_encoded_dyn ee = new Enum_encoded_dyn();
        int[] code = new int[arg.length()];
        char[] tmp = arg.toCharArray();
        for(int i = 0; i < tmp.length; i++) {
            code[i] = Character.getNumericValue(tmp[i]);
        }
        long start_t = System.nanoTime();
        int bingbong = ee.enum_code_dynprog(code, code.length);
        long end_t = System.nanoTime();
        System.out.print(" " + (end_t - start_t));
    }
}