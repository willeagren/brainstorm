/**
 *  Daily Coding Problem [7] - Facebook
 * 
 *  Given the mapping a = 1, b = 2, ..., z = 26, 
 *  and an encoded message, coun the number of 
 *  ways it can be decoded. 
 * 
 *  This is a recursive implementation.
 * 
 *  Author: Wilhelm Ågren, wagren@kth.se
 *  Last modified: 11/03/2020
 **/
public class Enum_encoded_rec {

    // uh oh stinky exponential time complexity :(
    private int enum_code_rec(int[] code, int n) {
        if(n == 1 || n == 0) return 1;
        int count = 0;
        if(code[n - 1] > 0) count = enum_code_rec(code, n - 1);
        if(code[n - 2] == 1 || code[n - 2] == 2 && code[n - 1] < 7) {
            count += enum_code_rec(code, n - 2);
        }
        return count;
    }

    public static void main(String[] args) {
        String arg = "";
        try {
            arg = args[0];
        } catch (Exception e) {
            System.err.println("[ERROR] Incorrect argument formatting...");
            return;
        }
        Enum_encoded_rec ee = new Enum_encoded_rec();
        int[] code = new int[arg.length()];
        char[] tmp = arg.toCharArray();
        for(int i = 0; i < tmp.length; i++) {
            code[i] = Character.getNumericValue(tmp[i]);
        }
        long start_t = System.nanoTime();
        int bingbong = ee.enum_code_rec(code, code.length);
        long end_t = System.nanoTime();
        long elapsed_t = end_t - start_t;
        System.out.print(" " + elapsed_t);
    }
}