/**
 *  Daily Coding Problem [2] - Uber
 * 
 *  Given an array of integers, return a new array such that 
 *  each element at index i of the new array is the product 
 *  of all the other numbers in the original array except i.
 * 
 *  Author: Wilhelm Ågren, wagren@kth.se
 *  Last modified: 07/03/2020
 **/
import java.util.*;
public class Index_product {

    // Func mod_list/1 takes an integer array as argument 
    // and returns a modified integer array according to
    // specifications in problem formulation. 
    //
    // Given an array l of length n,
    // Timecomplexity: T(n) ∈ O(n)
    // Memory complexity: Linear
    private int[] mod_list(int[] list) {
        int[] mod_list = new int[list.length];
        int prod = 1;
        for(int i : list) {
            prod *= i;
        }
        for(int i = 0; i < mod_list.length; i++) {
            mod_list[i] = prod/list[i];
        }
        return mod_list;
    }

    // Func mod_list_no_div/1 makes the same as the above function,
    // BUT does not utilize division. 
    //
    // Given an array l of length n,
    // Timecomplexity: T(n) ∈ O(n²)
    // Memory complexity: Linear
    private int[] mod_list_no_div(int[] list) {
        int[] mod_list = new int[list.length];
        for(int i = 0; i < mod_list.length; i++) {
            int prod = 1;
            for(int j : list) {
                if(j == list[i]) continue;
                prod *= j;
            }
            mod_list[i] = prod;
        }
        return mod_list;
    }

    // Func toString/1 takes an integer array and 
    // re-formats it to a string.
    private String toString(int[] l){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(int i : l) {
            if(i != l[l.length - 1]) {
                sb.append(i).append(",");
            } else {
                sb.append(i);
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public static void main(String[] args) {
        try {
            String[] in = args[0].split(",");
            Index_product ip = new Index_product();
            int[] l = new int[in.length];
            for(int i = 0; i < in.length; i++) {
                l[i] = Integer.parseInt(in[i]);
            }
            System.out.println(ip.toString(ip.mod_list_no_div(l)));

        } catch (Exception e) {
            System.err.println("[ERROR] Incorrect argument formating. Aborting...");
        }
    }
}