/**
 *  Daily Coding Problem [4] - Stripe
 * 
 *  Given an array of integers, find the missing positive 
 *  integer in linear time and constant space. In other words,
 *  find the lowest positive integer that does not exist in 
 *  the array. 
 * 
 *  Author: Wilhelm Ågren, wagren@kth.se
 *  Last modified: 07/03/2020
 **/
public class Low_int {

    // Func find_lowest/1 takes an integer array as argument
    // and returns the missing smallest integer
    // 
    // Given an array l of length n, 
    // Timecomplexity: T(n) ∈ O(n)
    // Memory complexity: Linear
    private int find_lowest(int[] l) {
        boolean[] map = new boolean[l.length];
        for(int i : l) {
            if(i < l.length && i >= 0) map[i] = true;
        }
        for(int i = 1; i < l.length; i++) {
            if(map[i] == false) return i;
        }
        return l.length;
    }
    public static void main(String[] args) {
        try {
            String[] arg = args[0].split(",");
            Low_int li = new Low_int();
            int[] l = new int[arg.length];
            for(int i = 0; i < l.length; i++) {
                l[i] = Integer.parseInt(arg[i]);
            }
            System.out.println("The missing positive integer is: [" + li.find_lowest(l) + "]");
        } catch(final Exception e) {
            System.err.println("[ERROR] Incorrect argument formating. Aborting...");
        }
    }
}