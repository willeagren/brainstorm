/**
 *  Daily Coding Problem [9] - Airbnb
 * 
 *  Given a list of integers, write a function
 *  that returns the largest sum of non-adjacent 
 *  numbers. Numbers can be 0 or negative.
 * 
 *  Author: Wilhelm Ågren, wagren@kth.se
 *  Last modified: 13/03/2020
 **/
import java.util.HashSet;
public class Non_adj_sum {

    // Func calc_adj_sum/1 takes the integer list as argument
    // and returns the largest sum of non-adjacent numbers.
    //
    // Given a list of length n
    // Timecomplexity: T(n) ∈ O(n)
    // Memory complexity is linear
    private int calc_adj_sum(int[] l) {
        int bool = 0;
        if(l.length == 1) {
            return l[0];
        }
        int sum_one = 0;
        int sum_two = 0;
        HashSet<Integer> hs_one = new HashSet<Integer>();
        HashSet<Integer> hs_two = new HashSet<Integer>();
        // Iterate over the list and add the integer to the 
        // correct HashSet.
        for(int i : l) {
            if(bool == 0) {
                hs_one.add(i);
                bool = bool ^ 1;
            } else {
                hs_two.add(i);
                bool = bool ^ 1;
            }
        }

        // Calculate sum of all elements in the HashSet
        for(int i : hs_one) {
            sum_one += i;
        }
        for(int i : hs_two) {
            sum_two += i;
        }

        // Return the largest sum from HashSet
        if(sum_one > sum_two) {
            return sum_one;
        } else {
            return sum_two;
        }
    }
    public static void main(String[] args) {
        String[] arg = {};
        try {
            arg = args[0].split(",");
        } catch (Exception e) {
            System.err.println("[ERROR] Incorrect argument formating...");
        }
        int[] l = new int[arg.length];
        for(int i = 0; i < l.length; i++) {
            l[i] = Integer.parseInt(arg[i]);
        }
        Non_adj_sum nas = new Non_adj_sum();
        System.out.println("The largest sum of non-adjacent numbers is: " + nas.calc_adj_sum(l));
    }
}