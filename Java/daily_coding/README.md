# Below you can read and see benchmarks of different implementations to the coding problems found in this _subdir_

## Benchmark of recursive strategy vs dynamic programing when calculating staircase combinatoric
In this _subdir_ you can find two programs called **Staircase_dyn.java** & **Staircase_rec.java**. These are programs to count the number of ways that a staircase can be climbed based on the number of steps; the first is implemented using a naive recursive _bottom-to-top_ approach that suffers from unnecessary re-calculations, and the latter is implemented using dynamic programing.

![Benchmarking comparison - dyn vs rec](Java/daily_coding/Bench/stair_dyn_vs_rec.png)
The image was generated using **gnuplot**; the green line is the recursive implementation and the purple line is the dynamic implementation. Y-axis is timescale and is currently set to nano-seconds, x-axis is the number of steps to compute.


## Benchmark of recursive strategy vs dynamic programing when decoding encoded messages
In this _subdir_ you can find two programs called **Enum_encoded_dyn.java** & **Enum_encoded_rec.java**. These are programs to count the number of ways an encoded message can be decoded; the first implemented using dynamic programing and the latter by using a naive recursive _bottom-to-top_ approach.

![Benchmarking comparison](Java/daily_coding/Bench/dyn_vs_rec.png)
The image was generated using **gnuplot**; the green line is the dynamic implementation and the purple line is the recursive implementation. X-axis is set to logscale as the length of the code is increased logarithmically. 


_The benchmarking programs used can be found at /Bench/._