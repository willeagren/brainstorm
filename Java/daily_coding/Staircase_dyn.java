/**
 *  Daily Coding Problem [12] - Amazon
 * 
 *  There exists a staircase with N steps, and you can
 *  climb either 1 or 2 steps at a time. Given N, 
 *  write a function that returns the number of unique ways 
 *  you can climb the staircase.
 * 
 *  Implementation using dynamic programming
 * 
 *  Author: Wilhelm Ågren, wagren@kth.se
 *  Last modified: 16/03/2020
 **/
public class Staircase_dyn {

    private int enum_dyn(int n) {
        int[] dyn = new int[n + 1];
        dyn[0] = 1;
        dyn[1] = 1;
        for(int i = 2; i <= n; i++) {
            dyn[i] = dyn[i - 1] + dyn[i - 2];
        }
        return dyn[n];
    }

    public static void main (String[] args) {
        try {
            int n = Integer.parseInt(args[0]);
            //String[] s = args[1].split(",");
            /*int[] l = new int[s.length];
            for(int i = 0; i < s.length; i++) {
                l[i] = Integer.parseInt(s[i]);
            }*/
            Staircase_dyn sd = new Staircase_dyn();
            long start_t = System.nanoTime();
            sd.enum_dyn(n);
            long end_t = System.nanoTime();
            long elapsed_t = end_t - start_t;
            System.out.print(" " + elapsed_t);
        } catch (Exception e) {
            System.err.println("[ERROR] Incorrect argument formating. Aborting...");
            return;
        }
    }
}