/**
 *  Daily Coding Problem [12] - Amazon
 * 
 *  There exists a staircase with N steps, and you can
 *  climb either 1 or 2 steps at a time. Given N, 
 *  write a function that returns the number of unique ways 
 *  you can climb the staircase.
 * 
 *  Implementation using recursion
 * 
 *  Author: Wilhelm Ågren, wagren@kth.se
 *  Last modified: 16/03/2020
 **/
public class Staircase_rec {

    private int enum_rec(int n) {
        if(n == 0 || n == 1) return 1;
        return enum_rec(n - 1) + enum_rec(n - 2);
    }

    public static void main (String[] args) {
        int n = 0;
        try {
            n = Integer.parseInt(args[0]);
        } catch (Exception e) {
            System.err.println("[ERROR] Incorrect argument formating. Aborting...");
            return;
        }
        Staircase_rec sr = new Staircase_rec();
        long start_t = System.nanoTime();
        sr.enum_rec(n);
        long end_t = System.nanoTime();
        long elapsed_t = end_t - start_t;
        System.out.print(" " + elapsed_t);
    }
}