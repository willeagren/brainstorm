/**
 *  Daily Coding Problem [1] - Google
 * 
 *  Given a list of numbers and a number k, return whether 
 *  any two numbers from the list add up to k.
 * 
 *  Author: Wilhelm Ågren, wagren@kth.se
 *  Last modified: 04/03/2020
 **/
import java.util.*;
public class Two_sum {

    // Func find_two_sum/2 takes an integer array l and an integer k
    // as arguments and returns an ArrayList that contains two
    // potential integers that sum up to the argument integer k.
    //
    // Given an array l of length n,
    // Timecomplexity: T(n) ∈ O(n)
    // Memory complexity: Linear worst case
    private ArrayList find_two_sum(int[] l, int k) {
        ArrayList al = new ArrayList();
        HashSet found = new HashSet();
        for(int i : l) {
            if(found.contains(k - i)) {
                al.add(k - i);
                al.add(i);
                return al;
            }
            found.add(i);
        }
        return al;
    }

    public static void main(String[] args) throws Exception {
        String set = "";
        String k = "";
        try {
            set = args[0];
            k = args[1];
        } catch(Exception e) {
            System.err.println("[ERROR] Incorrect input. \nAborting...");
        }
        Two_sum ts = new Two_sum();
        String[] s = set.split(",");
        int[] list = new int[s.length];
        for(int i = 0; i < list.length; i++) {
            list[i] = Integer.parseInt(s[i]);
        }
        ArrayList done = ts.find_two_sum(list, Integer.parseInt(k));
        if(done.isEmpty()) {
            System.out.println("[FALSE] No integers add up to [" + k + "] ");
        } else {
            System.out.print("[TRUE] " + k + " = [");
            for(int i = 0; i < done.size(); i++) {
                System.out.print(done.get(i));
                if(i != (done.size() - 1)) System.out.print("+");
            }
            System.out.println("]");
        }
    }
}