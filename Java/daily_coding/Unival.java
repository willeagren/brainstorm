/**
 *  Daily Coding Problem [8] - Google
 * 
 *  A unival tree is a tree where all nodes under it 
 *  have the same value.
 *  Given the root to a binary tree, count the number 
 *  of unival subtrees.
 * 
 *  Author: Wilhelm Ågren, wagren@kth.se
 *  Last modified: 14/03/2020
 **/
import java.util.*;
public class Unival {

    private class BST {

        private Node root;

        // Internal class for Binary Search Tree. Nodes build the tree
        private class Node {
            private int data;
            private Node left;
            private Node right;

            public Node(int data) {
                this.data = data;
                this.left = null;
                this.right = null;
            }
        }

        /*
        public BST(int root_data) {
            this.root = new Node(root_data);
        }*/

        public void add(int data) {
            root = add_node(root, data);
        }

        private Node add_node(Node cur, int data) {
            if(cur == null) return new Node(data);

            if(data <= cur.data) {
                cur.left = add_node(cur.left, data);
            } else {
                cur.right = add_node(cur.right, data);
            }
            return cur;
        }

        public void print_tree() {
            if(root == null) return;

            Queue<Node> q = new LinkedList<>();
            q.add(root);
            while(!q.isEmpty()) {
                Node node = q.remove();
                System.out.print(" " + node.data);

                if(node.left != null) q.add(node.left);
                if(node.right != null) q.add(node.right);
            }
            System.out.println();
        }

        public int unival_subtrees(Node n, int val) {
            int num = 0;
            if(n == null) return num;
            if(n.data == val) {
                num++;
                return num + unival_subtrees(n.left, val) + unival_subtrees(n.right, val);
            } else {
                return num + unival_subtrees(n.left, val) + unival_subtrees(n.right, val);
            }
        }
    }


    public static void main(String[] args) {
        Unival univ = new Unival();
        BST bst = univ.new BST();
        Scanner in = new Scanner(System.in);
        int c;
        while(true) {
            c = in.nextInt();
            if(c == 0) break;
            bst.add(c);
        }
        bst.print_tree();
        System.out.println(bst.unival_subtrees(bst.root, bst.root.data));
        in.close();
    }
}