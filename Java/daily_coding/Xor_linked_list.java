/**
 *  Daily Coding Problem [6] - Google
 * 
 *  An XOR linked list is a more memory efficient doubly linked list.
 *  Instead of each node holding 'next' and 'prev' field, it holds
 *  a field named 'both' which is an XOR of the next node and the 
 *  previous node. Implement this XOR linked list.
 * 
 *  Author: Wilhelm Ågren, wagren@kth.se
 *  Last modified: 09/03/2020
 **/
import java.util.*;
public class Xor_linked_list {

    // Tail node to know where the linked list starts
    private Node tail = null;

    // Private internal class Node used for the doubly linked list
    private class Node {

        // Node data, previous node and next node
        private int val;
        private Node next;
        private Node prev;

        // Constructor for Node that takes data for node as argument
        public Node(int v) {
            this.val = v;
            this.next = null;
            this.prev = null;
        }

        /*
        public int get_data() {
            return val;
        }*/

        /*
        public void set_data(int v) {
            this.val = v;
        }*/
    }

    // Func add_element/1 takes an integer data as argument 
    // and creates a new Node at the end of the list
    //
    // Given a list of length n:
    // Timecomplexity: T(n) ∈ O(n)
    private void add_element(int v) {
        Node new_node = new Node(v);
        if(tail == null) {
            tail = new_node;
            return;
        }
        Node tmp = tail;
        while(tmp.next != null) {
            tmp = tmp.next;
        }
        tmp.next = new_node;
        new_node.prev = tmp;
        return;
    }

    // Help function to print the doubly linked list in a pretty way
    private void print_list() {

        // Empty list
        if(tail == null) {
            System.out.println("There are no nodes in the linked list...");
            return;
        }
        Node tmp = tail;
        while(tmp.next != null) {
            System.out.print("[" + tmp.val + "]" + "<->");
            tmp = tmp.next;
        }
        System.out.print("[" + tmp.val + "]" + "\n");
        return;
    }

    // Used for debugging. LEGACY code
    /*
    private int len_of_list() {
        if(tail == null) return 0;
        int len = 1;
        Node tmp = tail;
        while(tmp.next != null) {
            len++;
            tmp = tmp.next;
        }
        return len;
    }*/

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Insert 0 to stop program\n");
        Scanner in = new Scanner(System.in);
        Xor_linked_list xli = new Xor_linked_list();
        int i;
        while(true) {
            System.out.println("\nPlease insert a node value: ");
            i = in.nextInt();
            if(i == 0) break;
            xli.add_element(i);
            System.out.println("\nThe doubly linked list looks like: ");
            //System.out.println("The number of nodes in the list are: " + xli.len_of_list());
            //Thread.sleep(500);
            xli.print_list();
        }
        System.out.println("Terminating process...");
        for(int x = 0; x < 10; x++) {
            Thread.sleep(500);
            System.out.println("|>");
        }
        in.close();
    }
}