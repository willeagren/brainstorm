public class Estimate_pi {

    private void estimate(int n) {
        System.out.println("--== [Estimating] ==--\n");
        int p_circle = 0;
        int p_square = 0;
        for(int i = 0; i < n; i++) {
            double x = Math.random();
            double y = Math.random();
            double z = (x*x + y*y);
            if(z <= 1) p_circle++;
            p_square++;
        }
        double pi = (double)(4*p_circle)/p_square;
        System.out.println("The value of pi was estimated to: " + pi);
    }
    
    public static void main(String[] args) {
        int n = 0;
        try{
            n = Integer.parseInt(args[0]);
        } catch (Exception e) {
            System.err.println("[ERR] Incorrect input parameter type.");
        }
        Estimate_pi pi = new Estimate_pi();
        pi.estimate(n);
    }
}