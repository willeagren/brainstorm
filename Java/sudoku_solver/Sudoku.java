/*
*   Java code that given a sudoku instance, produces the possible solutions for that instance.
*   User inputs the instance through STDin on terminal and solutions are ouputed on terminal.
*
*   Code utilizes the Kattio library for input/output. Source can be found at: https://github.com/Kattis/kattio and is used under the MIT lisence
*
*   Code written by Wilhelm Ågren. Email enquiries: wagren@kth.se
*   Last updated: 26/02/2020
*/
import java.io.BufferedReader; 
import java.io.IOException; 
import java.io.InputStreamReader; 
import java.util.*;
public class Sudoku {

    private final int TERMINAL_SIZE = 50;
    private boolean INPUT_CHECK = false;
    private int[][] board;

    // Func print_init/0 that will help guide user on how to input their puzzle instance for program to work correctly.
    private void print_init() {
        System.out.println("Welcome to [Birdbowl], the sudoku puzzle solver!\n\nYou will be asked to input your current sudoku instance that you want to solve.\nBelow will be an example of how to do that. Follow instructions for best result.");
        System.out.println("\n--== [Input example] ==--\n\n    -------------------\n     5 3 0 0 7 0 0 0 0\n     6 0 0 1 9 5 0 0 0\n     0 9 8 0 0 0 0 6 0\n     8 0 0 0 6 0 0 0 3\n     4 0 0 8 0 3 0 0 1\n     7 0 0 0 2 0 0 0 6\n     0 6 0 0 0 0 2 8 0\n     0 0 0 4 1 9 0 0 5\n     0 0 0 0 8 0 0 7 9\n    -------------------\n");
        System.out.println("Where a '0' means blank square; and there is an expected newline '\\n' or 'enter' in the terminal after each row.");
        System.out.println("\nNow please input your puzzle instance:\n\n--== [Reading input] ==--");
    }

    // Func read_input/0 takes user input and converts it to a sudoku puzzle board. Is then returned to main instance of class.
    private void read_input(Kattio io) throws IOException {

        // Initialize empty 9x9 board.
        board = new int[9][9];
        for(int y = 0; y < board.length; y++) {
            for(int x = 0; x < board[y].length; x++) {
                board[y][x] = io.getInt();
            }
        }
    }

    private void print_board() {
        System.out.print("\n    -------------------\n");
        for(int y = 0; y < board.length; y++) {
            System.out.print("    ");
            for(int x = 0; x < board[y].length; x++) {
                System.out.print(" " + board[y][x]);
            }
            System.out.print("\n");
        }
        System.out.print("    -------------------\n\n");
    }

    // Func print_board/1 takes sudoku instance, double int array, as argument and prints it for user. Ask user if satisfied with input, return answer true/false.
    private boolean query_user(Kattio io) throws IOException {
        
        // Clear terminal.
        for(int i = 0; i < TERMINAL_SIZE; i++) {
            System.out.print("\n");
        }

        // Print sudoku instance.
        System.out.println("The sudoku instance that you inserted looks like this:\n");
        print_board();

        // Ask user if satisfied with input. If satisfied, return true & continue; else return false & redo input step.
        System.out.print("Are you satisfied? Insert '1' in the terminal if you want to continue with this instance; or insert '2' to redo input step...\n");
        //Scanner sc = new Scanner(System.in);
        int a = 1;
        boolean valid_answer = false;
        boolean answer = false;
        while(!valid_answer) {
            a = io.getInt();
            switch(a) {
                case 1:     answer = true;
                            valid_answer = true;
                            break;
                case 2:     valid_answer = true;
                            break;
                default:    System.out.println("[ERR] Faulty input! Please insert valid command...");
                            break;
            }
        }
        return answer;
    }

    // Func possible_placement/4 takes (x,y) coordinates, number n and a board. Returns whether or not placement of n on (x,y) is allowed.
    private boolean possible_placement(int x, int y, int n) {

        //Search row and column for number n
        for(int i = 0; i < board.length; i++) {
            if(board[y][i] == n) return false;
        }
        for(int i = 0; i < board[x].length; i++) {
            if(board[i][x] == n) return false;
        }

        // Find floor values for x and y coordinates
        double xTmp = x/3;
        double yTmp = y/3;
        int x0 = 3*(int)Math.floor(xTmp);
        int y0 = 3*(int)Math.floor(yTmp);

        // Search square containing (x, y) for the number n
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                if(board[i + y0][j + x0] == n) return false;
            }
        }

        return true;
    }

    // Func solve_instance/1 takes the sudoku instance and solves it using recursion and backtracking. It is a brute force method.
    private boolean solve_instance() {
        for(int y = 0; y < board.length; y++) {
            for(int x = 0; x < board[y].length; x++) {
                if(board[y][x] == 0) {
                    for(int n = 1; n < 10; n++) {
                        if(possible_placement(x, y, n)) {
                            board[y][x] = n;
                            if(solve_instance()) {
                                return true;
                            } else {
                                board[y][x] = 0;
                            }
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }

    // Main function that intializes user-input handling and solving of puzzle.
    public static void main (String[] args) throws IOException {

        // Setup class instance
        Sudoku s = new Sudoku();
        Kattio io = new Kattio(System.in, System.out);
        
        // Print welcome & help message
        s.print_init();

        s.board = new int[9][9];

        // Generate user input
        s.read_input(io);
        /*while(!s.INPUT_CHECK) {
            s.INPUT_CHECK = s.query_user(io);
        }*/

        if(s.solve_instance()) {
            System.out.println("\n--== [Instance solved] ==--\n");
            s.print_board();
        } else {
            System.out.println("Could not solve instance... :^(");
        }

        io.flush();
        io.close();
    }
}