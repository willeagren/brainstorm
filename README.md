# What's the intended purpose of this git-repo?
I will be saving and storing all my various _algorithms_, coding interview solutions and _ideas_ in this git-repo from now on. If you feel like you want to use anything that is on here, you are free to do so - if you reference to this repo. Anything used from here is done so at your own risk...

```elixir
def bingbong(l = {:dong, []}) do :ding end
def bingbong(l = {:dong, [h|t]}) do [h + 1|bingbong({:dong, t})] end
```

## If you're not going to foo, then why bother with the zot?
* T(n) ∈ O(n*log(n)) for the bois, nothing else matters.
* Please running thread, suspend so that I can suspend suspending...